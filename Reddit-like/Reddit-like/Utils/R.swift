//
//  R.swift
//  Reddit-like
//
//  Created by Victory on 05/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import Foundation

struct R {
    
    static let share_view_comments = "View Comments"
    static let share_favorite = "Favorite"
    static let share_unfavorite = "Unfavorite"
    static let share_copy_url = "Copy URL"
    static let share_open = "Open in Safari"
    static let share_cencel = "Cancel"
    
    static let comment_post = "Post Comment"
    
    static let comment_reply = "Reply"
    static let comment_like = "Like"
    static let comment_dislike = "Dislike"
    static let comment_flag = "Flag"
    static let comment_cancel = "Cancel"
    
    static let alert_ok = "Ok"
    static let alert_cancel = "Cancel"
    
    static let required_name_field = "Username is required."
    static let input_correct_name = "Username should be 4-16 alpha-numeric charactors."
    static let input_your_email = "Please input your email address."
    static let input_correct_email = "Please enter the correct email address."
    static let input_password = "Enter a password, please."
    static let input_confirm_password = "Enter a password, again."
    static let password_length = "Password should be 8-16 charactors."
    static let add_photo = "Add your photo, please."
    
    static let input_password_mismatch = "Please enter new password and confirm password corrently."

    
    static let input_birthday = "Please input your birthday."
    
    static let required_location = "You didn't still get your location, please try again later."
    
    // preference key
    static let pref_user_id = "user_id"
    static let pref_user_name = "user_name"
    static let pref_user_email = "user_email"
    static let pref_user_password = "password"
    static let pref_user_token = "user_token"
    static let pref_expiration = "token_expiration"
    static let pref_user_image = "user_image"
    
    static let pref_auto_login  = "auto_log_in"
    
    static let pref_user_gender = "gender"
    static let pref_user_birthday = "birthday"
    
    static let pref_user_lat = "latitude"
    static let pref_user_lon = "longitude"
    
    static let input_comment_body = "Please input comment body"
    
    static let flag_comment_alert = "Are you sure to flag this comment?"
    
    
    static let OFFSET = 10
}
