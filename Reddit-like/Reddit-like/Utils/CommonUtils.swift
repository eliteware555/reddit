//
//  CommonUtils.swift
//  Reddit-like
//
//  Created by Victory on 13/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit

class CommonUtils {
    
    class func isValidEmail(email: String!) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(email)
    }
    
    // save image to a file (Documents/Reddit/profile.png)
    
    class func saveToFile(image: UIImage!) -> String! {
        
        var savedPhotoURL = ""
        
        let fileManager = NSFileManager.defaultManager()
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        
        var documentDirectory: NSString! = paths[0]
        
        // current document directory
        fileManager.changeCurrentDirectoryPath(documentDirectory as String)
        
        do {
            
            try fileManager.createDirectoryAtPath("Reddit", withIntermediateDirectories: true, attributes: nil)
            
        } catch let error as NSError {
            
            print(error.localizedDescription)
        }
        
        documentDirectory = documentDirectory.stringByAppendingPathComponent("Reddit")
        let filePath = documentDirectory.stringByAppendingPathComponent("profile.png")
        
        // if the file exists already, delete and write, else if create filePath
        
        if (fileManager.fileExistsAtPath(filePath)) {
            do {
                try fileManager.removeItemAtPath(filePath)
            }
            catch let error as NSError {
                print("Ooops! Something went wrong: \(error)")
            }
        } else {
            
            fileManager.createFileAtPath(filePath, contents: nil, attributes: nil)
        }
        
        if let data = UIImagePNGRepresentation(image) {
            
            data.writeToFile(filePath, atomically: true)
        }
        
        savedPhotoURL = filePath
        
        return savedPhotoURL
    }

    class func isAlphNumberic(phrase: String!) -> Bool {
        
        let letters = NSCharacterSet.letterCharacterSet()
        let digits = NSCharacterSet.decimalDigitCharacterSet()
        
        for uni in phrase.unicodeScalars {
            
            if (!letters.longCharacterIsMember(uni.value) && !digits.longCharacterIsMember(uni.value)) {
                
                return false
            }
        }
        
        return true
    }
    
    class func getSystemVersion() -> String {
    
        return UIDevice.currentDevice().systemName + " " + UIDevice.currentDevice().systemVersion
    }
    
    class func getAppVersion() -> String {
        
        return (NSBundle.mainBundle().infoDictionary?["CFBundleVersion"] as? String)!
    }
    
    
    class func getDeviceType() -> String {
    
        return UIDevice.currentDevice().mobileName
    }
    
    class func tokenExpired() -> Bool {
    
        // get expired date
        // change string to date
        let tokenExpiredDateString = UserDefault.getString(R.pref_expiration)
        
        if (tokenExpiredDateString == nil) {
            
            return false
            
        } else if (tokenExpiredDateString == "") {
            
            return false
        }
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let tokenExpiredDate = dateFormatter.dateFromString(tokenExpiredDateString!)
        
        // get current date
        let currentDate = NSDate()
        
        let timeInterval: Double = tokenExpiredDate!.timeIntervalSinceDate(currentDate)
        
        if (timeInterval <= 0.0) {
            
            return true
        }
        
        return false
    }
    
    /**
     Returns a percent-escaped string following RFC 3986 for a query string key or value.
     RFC 3986 states that the following characters are "reserved" characters.
     - General Delimiters: ":", "#", "[", "]", "@", "?", "/"
     - Sub-Delimiters: "!", "$", "&", "'", "(", ")", "*", "+", ",", ";", "="
     In RFC 3986 - Section 3.4, it states that the "?" and "/" characters should not be escaped to allow
     query strings to include a URL. Therefore, all "reserved" characters with the exception of "?" and "/"
     should be percent-escaped in the query string.
     - parameter string: The string to be percent-escaped.
     - returns: The percent-escaped string.
     */
    class func escape(string: String) -> String {
        
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        let allowedCharacterSet = NSCharacterSet.URLQueryAllowedCharacterSet().mutableCopy() as! NSMutableCharacterSet
        allowedCharacterSet.removeCharactersInString(generalDelimitersToEncode + subDelimitersToEncode)
        
        var escaped = ""
        
        //==========================================================================================================
        //
        //  Batching is required for escaping due to an internal bug in iOS 8.1 and 8.2. Encoding more than a few
        //  hundred Chinese characters causes various malloc error crashes. To avoid this issue until iOS 8 is no
        //  longer supported, batching MUST be used for encoding. This introduces roughly a 20% overhead. For more
        //  info, please refer to:
        //
        //      - https://github.com/Alamofire/Alamofire/issues/206
        //
        //==========================================================================================================
        
        if #available(iOS 8.3, OSX 10.10, *) {
            escaped = string.stringByAddingPercentEncodingWithAllowedCharacters(allowedCharacterSet) ?? string
        } else {
            let batchSize = 50
            var index = string.startIndex
            
            while index != string.endIndex {
                let startIndex = index
                let endIndex = index.advancedBy(batchSize, limit: string.endIndex)
                let range = startIndex..<endIndex
                
                let substring = string.substringWithRange(range)
                
                escaped += substring.stringByAddingPercentEncodingWithAllowedCharacters(allowedCharacterSet) ?? substring
                
                index = endIndex
            }
        }
        
        return escaped
    }
    
    
    class func getTopics() -> [String] {
        
        return ["U.S News", "World News", "Sports", "Business", "Science", "Tech", "Entertainment", "Arts", "Lifestyle"]
    }
    
    class func getSubTopics(topicIdx: Int!) -> [String] {
        
        switch topicIdx {
        case 0:
            
            return ["Politics", "Crime", "Education", "Immigration", "Jobs", "National Security"]
            
        case 1:
            
            return ["UK", "Europe", "Americas", "Asia", "Middle East", "Africa", "Australia"]
            
        case 2:
            
            return ["Football", "Basketball", "Baseball", "Hockey", "Soccer", "Tennis", "Golf", "Racing", "Cycling", "Flighting", "Cricket", "Track & Field", "Rugby", "Sailing", "Olympics"]
            
        case 3:
            
            return ["Apps", "Computers", "Games", "Wearables", "Gadgets"]
            
        case 4:
            
            return ["Wallstreet", "Economics", "Corporations", "Personal Finance", "Forex", "Start-ups"]
            
        case 5:
            
            return ["Environment", "Energy", "Climate Change", "Space", "Psychology"]
            
        case 6:
            
            return ["Movies", "TV", "Music", "Celebrities", "Gossip"]
            
        case 7:
            
            return ["Literature", "Stage", "Dance", "Paintings"]
            
        default:
            return ["Food", "Health & Fitness", "Travel", "Fashion", "Romance", "Family", "Home & Garden"]
        }
    }
    
    class func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ".characters)
        return String(text.characters.filter {okayChars.contains($0) })
    }
}

public extension UIDevice {
    
    var mobileName: String {
        
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 where value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
}
