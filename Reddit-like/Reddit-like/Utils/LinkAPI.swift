//
//  File.swift
//  Reddit-like
//
//  Created by Victory on 13/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LinkAPI {
    
    static let SERVER_URL = "http://cyclefieds.com/url/recommender.php"
    
    static let FAIL_CODE = 400
    static let FAIL_RESPONSE = 404
    static let SUCESS_CODE = 200
    static let VALID_TOKEN = 201
    static let EXISTING_TOKEN_SENT = 203
    static let NEW_TOKEN_SENT = 204
    static let UPLOAD_SUCCESS = 205
    
    /**
     *  @param  username    -   user name
     *  @param  useremail   -   user email address
     *  @param  password    -   password
     *  @param  image       - 
     *  @param  location    - 
     *  @param  device      - 
     *  @param  os          -
     
     *  @response 
     *  Success
     *      statusCode
     *      message
     *  Error
     *      statusCode
     *      message
    **/
    
    class func createUser(str_username: String!, str_useremail: String!, password: String!, birthday: String!, gender: String, device: String!, os: String!, appversion: String!, success: (statusCode: Int, message: String!)-> Void, failure: (statusCode: Int, message: String!) ->  Void) {
        
        let url = LinkAPI.SERVER_URL + "?page=createUser&" + "username=" + "\(str_username)" + "&password=" + "\(password)" + "&email=" + "\(str_useremail)" + "&birthday=" + "\(birthday)" + "&gender=" + "\(gender)" + "&device=" + "\(device)" + "&os=" + "\(os)" + "&appversion=" + "\(appversion)"
        
        Alamofire.request(.POST, url).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                let jsonResult = JSON(result)
                
                if (jsonResult["status"].int != nil) {
                    
                    if jsonResult["status"].int == 200 {
                        
                        success(statusCode: LinkAPI.SUCESS_CODE, message: jsonResult["message"].string!)
                        
                    }  else {
                        
                        failure(statusCode: LinkAPI.FAIL_RESPONSE, message: jsonResult["message"].string!)
                    }
                    
                } else {
                    
                    failure(statusCode: LinkAPI.FAIL_RESPONSE, message: "Connection to the server failed. \n Please try again")
                }
                
            case .Failure(let error):
                
                print(error)
                failure(statusCode: LinkAPI.FAIL_CODE, message: "Connection to the server failed. \n Please try again")
            }
        })
    }
    
    class func createUserWithLatAndLong(str_username: String!, str_useremail: String!, password: String!, birthday: String!, gender: String, lat: String!, long: String!, device: String!, os: String!, appversion: String!, success: (statusCode: Int, message: String!)-> Void, failure: (statusCode: Int, message: String!) ->  Void) {
        
        var url = ""
        
        if ( lat == nil || long == nil) {
            
            url = LinkAPI.SERVER_URL + "?page=createUser&" + "username=" + "\(str_username)" + "&password=" + "\(password)" + "&email=" + "\(str_useremail)" + "&birthday=" + "\(birthday)" + "&gender=" + "\(gender)" + "&device=" + "\(device)" + "&os=" + "\(os)" + "&appversion=" + "\(appversion)"
            
        } else {
        
            url = LinkAPI.SERVER_URL + "?page=createUser&" + "username=" + "\(str_username)" + "&password=" + "\(password)" + "&email=" + "\(str_useremail)" + "&birthday=" + "\(birthday)" + "&gender=" + "\(gender)" + "&lat=" + "\(lat)" + "&long=" + "\(long)" + "&device=" + "\(device)" + "&os=" + "\(os)" + "&appversion=" + "\(appversion)"
        }
        
        Alamofire.request(.POST, url).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                let jsonResult = JSON(result)
                
                if (jsonResult["status"].int != nil) {
                    
                    if jsonResult["status"].int == 200 {
                        
                        success(statusCode: LinkAPI.SUCESS_CODE, message: jsonResult["message"].string!)
                        
                    }  else {
                        
                        failure(statusCode: LinkAPI.FAIL_RESPONSE, message: jsonResult["message"].string!)
                    }
                    
                } else {
                    
                    failure(statusCode: LinkAPI.FAIL_RESPONSE, message: "Connection to the server failed. \n Please try again")
                }
                
            case .Failure(let error):
                
                print(error)
                failure(statusCode: LinkAPI.FAIL_CODE, message: "Connection to the server failed. \n Please try again")
            }
        })
    }
    
    class func getToken(username: String!, password: String!, email: String!, success: (statusCode: Int, message: String!, user_id: Int, token: String!, tokenExpiration: String!)-> Void, failure: (statusCode: Int, message: String!) ->  Void) {
        
        let url = LinkAPI.SERVER_URL + "?page=getToken&" + "username=" + "\(username)" + "&password=" + "\(password)" + "&email=" + "\(email)"
        
        Alamofire.request(.POST, url).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                let jsonResult = JSON(result)
                
                if (jsonResult["status"].int != nil) {
                    
                    if (jsonResult["status"].int == EXISTING_TOKEN_SENT || jsonResult["status"].int == NEW_TOKEN_SENT) {
                        
                        success(statusCode: LinkAPI.SUCESS_CODE, message: jsonResult["message"].string!, user_id: Int(jsonResult["userid"].string!)!, token: jsonResult["token"].string!, tokenExpiration: jsonResult["tokenExpiration"].string!)
                        
                    }  else {
                        
                        failure(statusCode: LinkAPI.FAIL_RESPONSE, message: jsonResult["message"].string!)
                    }
                    
                } else {
                    
                    failure(statusCode: LinkAPI.FAIL_RESPONSE, message: "Connection to the server failed. \n Please try again")
                }
                
            case .Failure(let error):
                
                print(error)
                failure(statusCode: LinkAPI.FAIL_CODE, message: "Connection to the server failed. \n Please try again")
            }
        })
    }
    
    class func verifyToken(user_id: Int, token: String!, success: (statusCode: Int, message: String!)-> Void, failure: (statusCode: Int, message: String!) ->  Void) {
        
        let url = LinkAPI.SERVER_URL + "?page=verifyToken&" + "userid=" + "\(user_id)" + "&token=" + "\(token)"
        
        Alamofire.request(.POST, url).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                let jsonResult = JSON(result)
                
                if (jsonResult["status"].int != nil) {
                    
                    if jsonResult["status"].int == VALID_TOKEN {
                        
                        success(statusCode: LinkAPI.SUCESS_CODE, message: jsonResult["message"].string!)
                        
                    }  else {
                        
                        failure(statusCode: LinkAPI.FAIL_RESPONSE, message: jsonResult["message"].string!)
                    }
                    
                } else {
                    
                    failure(statusCode: LinkAPI.FAIL_RESPONSE, message: "Connection to the server failed. \n Please try again")
                }
                
            case .Failure(let error):
                
                print(error)
                failure(statusCode: LinkAPI.FAIL_CODE, message: "Connection to the server failed. \n Please try again")
            }
        })
    }
    
    class func verifyCredentials(email: String!, password: String!, success: (statusCode: Int, message: String!, user_id: Int, user_name: String!, token: String!, tokenExpiration: String!)-> Void, failure: (statusCode: Int, message: String!) ->  Void) {
        
        let url = LinkAPI.SERVER_URL + "?page=getToken&" + "email=" + "\(email)" + "&password=" + "\(password)"
        
        Alamofire.request(.POST, url).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                let jsonResult = JSON(result)
                
                if (jsonResult["status"].int != nil) {
                    
                    if (jsonResult["status"].int == EXISTING_TOKEN_SENT || jsonResult["status"].int == NEW_TOKEN_SENT) {
                        
                        success(statusCode: LinkAPI.SUCESS_CODE, message: jsonResult["message"].string!, user_id: Int(jsonResult["userid"].string!)!, user_name: jsonResult["username"].string!, token: jsonResult["token"].string!, tokenExpiration: jsonResult["tokenExpiration"].string!)
                        
                    }  else {
                        
                        failure(statusCode: LinkAPI.FAIL_RESPONSE, message: jsonResult["message"].string!)
                    }
                    
                } else {
                    
                    failure(statusCode: LinkAPI.FAIL_RESPONSE, message: "Connection to the server failed. \n Please try again")
                }
                
            case .Failure(let error):
                
                print(error)
                failure(statusCode: LinkAPI.FAIL_CODE, message: "Connection to the server failed. \n Please try again")
            }
        })
    }
    
    class func addUserImage(user_id: Int, token: String, image_url: String, success: (statusCode: Int, message: String!)-> Void, failure: (statusCode: Int, message: String!) ->  Void) {
        
        Alamofire.upload(
            .POST,
            LinkAPI.SERVER_URL,
            multipartFormData: { multipartFormData in
                multipartFormData.appendBodyPart(fileURL: NSURL(fileURLWithPath: image_url), name: "image")
                multipartFormData.appendBodyPart(data: "addUserImage".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name: "page")
                multipartFormData.appendBodyPart(data: "\(user_id)".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name: "userid")
                multipartFormData.appendBodyPart(data: "\(token)".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name: "token")
            },
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .Success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        debugPrint(response)
                        
                        switch response.result {
                            
                        case .Success(let result):
                            
                            let jsonResult = JSON(result)
                            
                            if (jsonResult["status"].int != nil) {
                                
                                if (jsonResult["status"].int == 205) {
                                    
                                    success(statusCode: LinkAPI.SUCESS_CODE, message: jsonResult["message"].string!)
                                    
                                }  else {
                                    
                                    failure(statusCode: LinkAPI.FAIL_RESPONSE, message: jsonResult["message"].string!)
                                }
                                
                            } else {
                                
                                failure(statusCode: LinkAPI.FAIL_RESPONSE, message: "Connection to the server failed. \n Please try again")
                            }
                            
                        case .Failure(let error):
                            
                            print(error)
                            
                            failure(statusCode: LinkAPI.FAIL_RESPONSE, message: "Connection to the server failed. \n Please try again")
                        }
                    }
                case .Failure(let encodingError):
                    
                    print(encodingError)
                    
                    failure(statusCode: LinkAPI.FAIL_CODE, message: "Connection to the server failed. \n Please try again")
                }
            }
        )
    }
    
    class func getUserImage(user_id: Int, token: String!, success: (statusCode: Int, message: String!)-> Void, failure: (statusCode: Int, message: String!) ->  Void) {
        
        let url = LinkAPI.SERVER_URL + "?page=getUserImage&" + "userid=" + "\(user_id)" + "&token=" + "\(token)"
        
        Alamofire.request(.POST, url).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                let jsonResult = JSON(result)
                
                if (jsonResult["status"].int != nil) {
                    
                    if jsonResult["status"].int <= 299 && jsonResult["status"].int >= 200 {
                        
                        UserDefault.setString(jsonResult["image"].string!, key: R.pref_user_image)
                        
                        success(statusCode: LinkAPI.SUCESS_CODE, message: jsonResult["message"].string!)                       
                        
                        
                    }  else {
                        
                        failure(statusCode: LinkAPI.FAIL_RESPONSE, message: jsonResult["message"].string!)
                    }
                    
                } else {
                    
                    failure(statusCode: LinkAPI.FAIL_RESPONSE, message: "Connection to the server failed. \n Please try again")
                }
                
            case .Failure(let error):
                
                print(error)
                failure(statusCode: LinkAPI.FAIL_CODE, message: "Connection to the server failed. \n Please try again")
            }
        })
    }
    
    class func getLinks(user_id: Int!, token: String!, type: String!, offset: Int, length: Int!, sortCategory: String!, sucess: (message:[LinkModel])->Void, failure:(message: String!) -> Void) {
        
        
        print("getLinks = " + "type - " + "\(type)")
        
        let url = LinkAPI.SERVER_URL + "?page=getLinks" + "&userid=" + "\(user_id)" + "&token=" + "\(token)" + "&type=" + "\(type)" + "&offset=" + "\(offset)" + "&length=" + "\(length)" + "&sortCategory=" + "\(sortCategory)"
        
        Alamofire.request(.POST, url).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                let jsonResult = JSON(result)
                
                var linkList: [LinkModel] = []

                if let linkCount = jsonResult.array?.count {

                    if (linkCount > 0) {

                        for index in 0 ..< linkCount {

                            let linkModel = LinkModel()

                            linkModel.linkid = jsonResult[index]["linkid"].int!
                            linkModel.linkedURL = jsonResult[index]["url"].string!
                            linkModel.title = jsonResult[index]["title"].string!
                            linkModel.source = jsonResult[index]["source"].string!
                            linkModel.subDetail = jsonResult[index]["description"].string!
                            linkModel.imageURL = jsonResult[index]["image"].string!
                            linkModel.commentCnt = jsonResult[index]["commentCount"].int!

                            linkModel.views = jsonResult[index]["views"].int!
                            linkModel.score = jsonResult[index]["score"].int!
                            linkModel.likeValue = jsonResult[index]["likeValue"].int!
                            linkModel.isFavorite = jsonResult[index]["isFavorite"].bool!
                            linkModel.shortdate = jsonResult[index]["shortdate"].string!
                            linkModel.pubdate = jsonResult[index]["pubdate"].string!
                            linkModel.rssid = jsonResult[index]["rssid"].int!
                            
                            linkList.append(linkModel)
                        }
                    }
                }
                
                sucess(message: linkList)
                
            case .Failure(let error):
                
                print(error)
                failure(message: "Connection to the server failed. \n Please try again")
            }
        })
    }
    
    class func getLinksWithQuery(user_id: Int!, token: String!, type: String!, offset: Int!, length: Int!, sortCategory: String!, query:  String!, sucess: (message:[LinkModel]) -> Void, failure: (message: String!) -> Void) {
        
        print("getLinks = " + "type - " + "\(type)" + ", query - " + "\(query)")
        
        let url = LinkAPI.SERVER_URL + "?page=getLinks" + "&userid=" + "\(user_id)" + "&token=" + "\(token)" + "&type=" + "\(type)" + "&offset=" + "\(offset)" + "&length=" + "\(length)" + "&sortCategory=" + "\(sortCategory)" + "&query=" + query
        
        Alamofire.request(.POST, url).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                let jsonResult = JSON(result)
                
                var linkList: [LinkModel] = []
                
                if let linkCount = jsonResult.array?.count {
                    
                    if (linkCount > 0) {
                        
                        for index in 0 ..< linkCount {
                            
                            let linkModel = LinkModel()
                            
                            linkModel.linkid = jsonResult[index]["linkid"].int!
                            linkModel.linkedURL = jsonResult[index]["url"].string!
                            linkModel.title = jsonResult[index]["title"].string!
                            linkModel.source = jsonResult[index]["source"].string!
                            linkModel.subDetail = jsonResult[index]["description"].string!
                            linkModel.imageURL = jsonResult[index]["image"].string!
                            linkModel.commentCnt = jsonResult[index]["commentCount"].int!
                            
                            linkModel.views = jsonResult[index]["views"].int!
                            linkModel.score = jsonResult[index]["score"].int!
                            linkModel.likeValue = jsonResult[index]["likeValue"].int!
                            linkModel.isFavorite = jsonResult[index]["isFavorite"].bool!
                            linkModel.shortdate = jsonResult[index]["shortdate"].string!
                            linkModel.pubdate = jsonResult[index]["pubdate"].string!
                            linkModel.rssid = jsonResult[index]["rssid"].int!
                            
                            linkList.append(linkModel)
                        }
                    }
                }
                
                sucess(message: linkList)
                
            case .Failure(let error):
                
                print(error)
                failure(message: "Connection to the server failed. \n Please try again")
            }
        })
    }
    
    class func setLinkAsViewed(user_id: Int!, token: String!, linkid: Int!, success: (statusCode: Int, message: String!)-> Void, failure: (statusCode: Int, message: String!) ->  Void) {
        
        let url = LinkAPI.SERVER_URL + "?page=setLinkAsViewed&" + "userid=" + "\(user_id)" + "&token=" + "\(token)" + "&linkid=" + "\(linkid)"
        
        Alamofire.request(.POST, url).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                let jsonResult = JSON(result)
                
                if (jsonResult["status"].int != nil) {
                    
                    if jsonResult["status"].int <= 299 && jsonResult["status"].int >= 200 {
                        
                        success(statusCode: LinkAPI.SUCESS_CODE, message: jsonResult["message"].string!)
                        
                    }  else {
                        
                        failure(statusCode: LinkAPI.FAIL_RESPONSE, message: jsonResult["message"].string!)
                    }
                    
                } else {
                    
                    failure(statusCode: LinkAPI.FAIL_RESPONSE, message: "Connection to the server failed. \n Please try again")
                }
                
            case .Failure(let error):
                
                print(error)
                failure(statusCode: LinkAPI.FAIL_CODE, message: "Connection to the server failed. \n Please try again")
            }
        })
    }
    
    
    class func resetPassword(email: String!, success: (statusCode: Int, message: String!)-> Void, failure: (statusCode: Int, message: String!) ->  Void) {
        
        let url = LinkAPI.SERVER_URL + "?page=resetPassword&" + "email=" + "\(email)"        
        
        Alamofire.request(.POST, url).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                let jsonResult = JSON(result)
                
                if (jsonResult["status"].int != nil) {
                    
                    if jsonResult["status"].int <= 299 && jsonResult["status"].int >= 200 {
                        
                        success(statusCode: LinkAPI.SUCESS_CODE, message: jsonResult["message"].string!)
                        
                    }  else {
                        
                        failure(statusCode: LinkAPI.FAIL_RESPONSE, message: jsonResult["message"].string!)
                    }
                    
                } else {
                    
                    failure(statusCode: LinkAPI.FAIL_RESPONSE, message: "Connection to the server failed. \n Please try again")
                }
                
            case .Failure(let error):
                
                print(error)
                failure(statusCode: LinkAPI.FAIL_CODE, message: "Connection to the server failed. \n Please try again")
            }
        })
    }
    
    class func setUserLinkFavorite(user_id: Int!, token: String!, linkid: Int!, isFavorite: Bool!, success: (statusCode: Int, message: String!)-> Void, failure: (statusCode: Int, message: String!) ->  Void) {
        
        let url = LinkAPI.SERVER_URL + "?page=setUserLinkFavorite&" + "userid=" + "\(user_id)" + "&token=" + "\(token)" + "&linkid=" + "\(linkid)" + "&isFavorite=" + "\(isFavorite)"
        
        Alamofire.request(.POST, url).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                let jsonResult = JSON(result)
                
                if (jsonResult["status"].int != nil) {
                    
                    if jsonResult["status"].int <= 299 && jsonResult["status"].int >= 200 {
                        
                        success(statusCode: LinkAPI.SUCESS_CODE, message: jsonResult["message"].string!)
                        
                    }  else {
                        
                        failure(statusCode: LinkAPI.FAIL_RESPONSE, message: jsonResult["message"].string!)
                    }
                    
                } else {
                    
                    failure(statusCode: LinkAPI.FAIL_RESPONSE, message: "Connection to the server failed. \n Please try again")
                }
                
            case .Failure(let error):
                
                print(error)
                failure(statusCode: LinkAPI.FAIL_CODE, message: "Connection to the server failed. \n Please try again")
            }
        })
    }
    
    class func updateUserLink(user_id: Int!, token: String!, linkid: Int!, likevalue: Int!, success: (statusCode: Int, message: String!)-> Void, failure: (statusCode: Int, message: String!) ->  Void) {
        
        let url = LinkAPI.SERVER_URL + "?page=updateUserLink&" + "userid=" + "\(user_id)" + "&token=" + "\(token)" + "&linkid=" + "\(linkid)" + "&likevalue=" + "\(likevalue)"
        
        Alamofire.request(.POST, url).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                let jsonResult = JSON(result)
                
                if (jsonResult["status"].int != nil) {
                    
                    if jsonResult["status"].int <= 299 && jsonResult["status"].int >= 200 {
                        
                        success(statusCode: LinkAPI.SUCESS_CODE, message: jsonResult["message"].string!)
                        
                    }  else {
                        
                        failure(statusCode: LinkAPI.FAIL_RESPONSE, message: jsonResult["message"].string!)
                    }
                    
                } else {
                    
                    failure(statusCode: LinkAPI.FAIL_RESPONSE, message: "Connection to the server failed. \n Please try again")
                }
                
            case .Failure(let error):
                
                print(error)
                failure(statusCode: LinkAPI.FAIL_CODE, message: "Connection to the server failed. \n Please try again")
            }
        })
    }
    
    class func getComments(userid: Int!, token: String!, linkid: Int!, sortCategory: String!, sortOrder: String!, sucess: (message:[CommentModel])->Void, failure:(message: String!) -> Void) {
        
        let url = LinkAPI.SERVER_URL + "?page=getComments" + "&userid=" + "\(userid)" + "&token=" + "\(token)" + "&linkid=" + "\(linkid)" + "&sortCategory=" + "\(sortCategory)" + "&sortOrder=" + "\(sortOrder)"
        
        Alamofire.request(.POST, url).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                let jsonResult = JSON(result)
                
                var commentList: [CommentModel] = []
                
                if let linkCount = jsonResult.array?.count {
                    
                    if (linkCount > 0) {
                        
                        for index in 0 ..< linkCount {
                            
                            commentList.append(setCommentModel(index, value: jsonResult))
                        }
                    }
                }
                
                sucess(message: commentList)
                
            case .Failure(let error):
                
                print(error)
                failure(message: "Connection to the server failed. \n Please try again")
            }
        })
    }
    
    class func setCommentModel(index: Int, value: JSON) -> CommentModel {
        
        let commentModel = CommentModel()
        
        commentModel.commentId = Int(value[index]["commentid"].string!)
        commentModel.linkId = Int(value[index]["linkid"].string!)
        commentModel.userId = Int(value[index]["userid"].string!)
        commentModel.user_name = value[index]["username"].string!
        
        commentModel.comment_body = value[index]["body"].string!
        commentModel.comment_date = value[index]["date"].string!
        commentModel.comment_score = value[index]["score"].string!
        
        commentModel.comment_shortdate = value[index]["shortdate"].string!
        
        // user image
        commentModel.user_image_url = value[index]["image"].string!
        
        if let repliesCount = value[index]["replies"].array?.count {
            
            if (repliesCount > 0) {
                
                commentModel.replies = setCommentReplies(value[index]["replies"])
            }
        }
        
        return commentModel
    }
    
    class func setCommentReplies(value: JSON) -> [CommentModel] {
        
        var replies: [CommentModel] = []
        
        
        for index in 0 ..<  value.array!.count {
            
            replies.append(setCommentModel(index, value: value))
            
        }
        
        return replies
    }
    
    class func postComment(userid: Int!, token: String!, linkid: Int!, replyToCommentid: String!, body: String!, success: (message: String!) -> Void, failure: (message: String!) -> Void) {
        
        var url = ""
        
        let encodedBody = CommonUtils.escape(body)
        
        print(encodedBody)
        
        if (replyToCommentid == "") {
            
            url = LinkAPI.SERVER_URL + "?page=postComment" + "&userid=" + "\(userid)" + "&token=" + "\(token)" + "&linkid=" + "\(linkid)" + "&body=" + "\(encodedBody)"
            
        } else {
            
            url = LinkAPI.SERVER_URL + "?page=postComment" + "&userid=" + "\(userid)" + "&token=" + "\(token)" + "&linkid=" + "\(linkid)" + "&replyToCommentid=" + replyToCommentid + "&body=" + "\(encodedBody)"
        }
        
        Alamofire.request(.POST, url).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                let jsonResult = JSON(result)
                
                if (jsonResult["status"].int != nil) {
                    
                    if jsonResult["status"].int <= 299 && jsonResult["status"].int >= 200 {
                        
                        success(message: jsonResult["message"].string!)
                        
                    }  else {
                        
                        failure(message: jsonResult["message"].string!)
                    }
                    
                } else {
                    
                    failure(message: "Connection to the server failed. \n Please try again")
                }
                
            case .Failure(let error):
                
                print(error)
                failure(message: "Connection to the server failed. \n Please try again")
            }
        })
    }
    
    class func updateUserCommentScore(userid: Int!, token: String!, commentid: Int!, likedvalue: Int!, success: (message: String!) -> Void, failure: (message: String!) -> Void) {
        
        let url = LinkAPI.SERVER_URL + "?page=updateUserCommentScore" + "&userid=" + "\(userid)" + "&token=" + "\(token)" + "&commentid=" + "\(commentid)" + "&likedvalue=" + "\(likedvalue)"
        
        Alamofire.request(.POST, url).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                let jsonResult = JSON(result)
                
                if (jsonResult["status"].int != nil) {
                    
                    if jsonResult["status"].int <= 299 && jsonResult["status"].int >= 200 {
                        
                        success(message: jsonResult["message"].string!)
                        
                        
                    }  else {
                        
                        failure(message: jsonResult["message"].string!)
                    }
                    
                } else {
                    
                    failure(message: "Connection to the server failed. \n Please try again")
                }
                
            case .Failure(let error):
                
                print(error)
                failure(message: "Connection to the server failed. \n Please try again")
            }
        })
    }
    
    class func flagComment(userid: Int!, token: String!, commentid: Int!, success: (message: String!) -> Void, failure: (message: String!) -> Void) {
        
        let url = LinkAPI.SERVER_URL + "?page=flagComment" + "&userid=" + "\(userid)" + "&token=" + "\(token)" + "&commentid=" + "\(commentid)"
        
        Alamofire.request(.POST, url).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                let jsonResult = JSON(result)
                
                if (jsonResult["status"].int != nil) {
                    
                    if jsonResult["status"].int <= 299 && jsonResult["status"].int >= 200 {
                        
                        success(message: jsonResult["message"].string!)
                        
                    }  else {
                        
                        failure(message: jsonResult["message"].string!)
                    }
                    
                } else {
                    
                    failure(message: "Connection to the server failed. \n Please try again")
                }
                
            case .Failure(let error):
                
                print(error)
                failure(message: "Connection to the server failed. \n Please try again")
            }
        })
    }
    
    class func updateUserTopics(user_id: Int, token: String, topics: [TopicModel], success: (message: String) -> Void, failure: (message: String) -> Void) {
        
        var url = LinkAPI.SERVER_URL + "?page=updateUserTopics" + "&userid=" + "\(user_id)" + "&token=" + "\(token)"
        
        for index in 0 ..< topics.count {
            
            let subTopic = CommonUtils.removeSpecialCharsFromString(topics[index].item).lowercaseString
            
            url +=  "&topics[" + "\(index)" + "]=" + "\(subTopic)"
        }
        
        print(url)
        
        Alamofire.request(.POST, url).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                let jsonResult = JSON(result)
                
                if (jsonResult["status"].int != nil) {
                    
                    if jsonResult["status"].int <= 299 && jsonResult["status"].int >= 200 {
                        
                        success(message: jsonResult["message"].string!)
                        
                    }  else {
                        
                        failure(message: jsonResult["message"].string!)
                    }
                    
                } else {
                    
                    failure(message: "Connection to the server failed. \n Please try again")
                }
                
            case .Failure(let error):
                
                print(error)
                failure(message: "Connection to the server failed. \n Please try again")
            }
        })
    }
    
    class func changePassword(email: String!, oldPassword: String!, newPassword: String!, success: (message: String) -> Void, failure: (message: String) -> Void) {
        
        let old_password = CommonUtils.escape(oldPassword)
        let new_password = CommonUtils.escape(newPassword)
        
        let url = LinkAPI.SERVER_URL + "?page=changePassword&" + "email=" + "\(email)" + "&oldPassword=" + "\(old_password)" + "&newPassword=" + "\(new_password)"
        
        Alamofire.request(.POST, url).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                let jsonResult = JSON(result)
                
                if (jsonResult["status"].int != nil) {
                    
                    if jsonResult["status"].int <= 299 && jsonResult["status"].int >= 200 {
                        
                        print(jsonResult["token"].string!)
                        print(jsonResult["tokenExpiration"].string!)
                        
                        UserDefault.setString(jsonResult["token"].string!, key: R.pref_user_token)
                        UserDefault.setString(jsonResult["tokenExpiration"].string!, key: R.pref_expiration)
                        
                        success(message: jsonResult["message"].string!)
                        
                    }  else {
                        
                        failure(message: jsonResult["message"].string!)
                    }
                    
                } else {
                    
                    failure(message: "Connection to the server failed. \n Please try again")
                }
                
            case .Failure(let error):
                
                print(error)
                failure(message: "Connection to the server failed. \n Please try again")
            }
        })
    }
}



