//
//  UserDefault.swift
//  Reddit-like
//
//  Created by Victory on 13/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit

class UserDefault {
    
    static let prefs = NSUserDefaults.standardUserDefaults()
    
    static func setString(value: String!, key: String!) {
        
        prefs.setObject(value, forKey: key)
        prefs.synchronize()
    }
    
    static func getString(key: String!) -> String? {
        
        return prefs.objectForKey(key) as! String?
    }
    
    static func setIntValue(value: Int!, key: String!) {
        
        prefs.setInteger(value, forKey: key)
        prefs.synchronize()
    }
    
    static func getIntValue(key: String!) -> Int! {
        
        return prefs.objectForKey(key) as! Int!
    }
    
    static func setBoolValue(value: Bool!, key: String!) {
        
        prefs.setBool(value, forKey: key)
        prefs.synchronize()
    }
    
    static func getBoolValue(key: String) -> Bool! {
        
        return prefs.objectForKey(key) as! Bool!
    }
    
    static func setBoolDefaultValue(value: Bool!, key: String!) {
        
        prefs.registerDefaults([key : value])
    }
}
