//
//  AppDelegate.swift
//  Reddit-like
//
//  Created by Victory on 04/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

    var window: UIWindow?
    
    // progress
    var HUD: MBProgressHUD?
    
    var locationManager: CLLocationManager! = nil
    
    var userLat: String! = nil
    var userLon: String! = nil

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        UserDefault.setBoolDefaultValue(false, key: R.pref_auto_login)
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        stopLocationService()
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        startLocationService()
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // show loading view with title
    func showLoadingViewWithTitle(title: String!, sender: AnyObject) {
    
        HUD = MBProgressHUD(view: self.window)
        self.window?.addSubview(HUD!)
        
        HUD?.color = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 0.75)
        
        HUD!.labelText = title
        HUD?.show(true)
    }
    
    // hide loading view
    func hideLoadingView() {
        
        HUD?.hide(true)
    }
    
    // hide loading view after delay
    func hideLoadingView(delay: NSTimeInterval!) {
        
        HUD?.hide(true, afterDelay: delay)
    }
    
    // show AlertViewController
    func showAlertDialog(title: String?, message: String?, positive: String?, negative: String?, sender: AnyObject?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        if (positive != nil) {
        
            alert.addAction(UIAlertAction(title: positive, style: UIAlertActionStyle.Default, handler: nil))
        }
        
        if (negative != nil) {
            alert.addAction(UIAlertAction(title: negative, style: UIAlertActionStyle.Default, handler: nil))
        }
        
        (sender as! UIViewController).presentViewController(alert, animated: true, completion: nil)
    }
    
    // MARK: Location Service Utils
    
    func startLocationService() {
        
        locationManager = CLLocationManager()
        
        if (UIDevice.currentDevice().systemVersion as NSString).floatValue >= 8.0 {
            if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.NotDetermined {
                locationManager.requestWhenInUseAuthorization()
            }
        }
        
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }
    
    func stopLocationService() {
        
        if (locationManager != nil) {
        
            locationManager.delegate = nil
            locationManager.stopUpdatingLocation()
        }
    }
    
    // MARK: - CLLocationManager Delegate
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        
        userLat = "\(newLocation.coordinate.latitude as Double)"
        userLon = "\(newLocation.coordinate.longitude as Double)"
        
        print("latitude: " + userLat + ", longitude: " + userLon)
        
        UserDefault.setString("\(newLocation.coordinate.latitude as Double)", key: R.pref_user_lat)
        UserDefault.setString("\(newLocation.coordinate.longitude as Double)", key: R.pref_user_lon)
        
        stopLocationService()
    }
}




