//
//  DraggableViewBackground.swift
//  TinderSwipeCardsSwift
//
//  Created by Gao Chao on 4/30/15.
//  Copyright (c) 2015 gcweb. All rights reserved.
//

import Foundation
import UIKit
import JLToast


protocol DraggableViewBackgroundDelegate {
    
    func gotoWebView(model: LinkModel!) -> Void
    
    func gotoCommentView(link: LinkModel!) -> Void
}

class DraggableViewBackground: UIView, DraggableViewDelegate {
    
    var homeOffSet = 0
    
    var delegate: DraggableViewBackgroundDelegate!
    
    // The loadedCard list buffer size
    // WARNING: This value must never be below the number of links the server will return
    let MAX_LOADED_CARD_SIZE = 3
    
    // The amount of unloaded links to fetch more links from the server.
    // WARNING: This value must never be below MAX_LOADED_CARD_SIZE and the number of links the server will return.
    let LINKAPI_FETCH_SIZE_TRIGGER = 2
    
    let CARD_HEIGHT: CGFloat = 340
    let CARD_WIDTH: CGFloat = 280
   
    var currentIdx = -1
    
    var loadedCards: [DraggableView]!
    
    var homeList = [LinkModel]()
    
    var menuButton: UIButton!
    var messageButton: UIButton!
    var checkButton: UIButton!
    var xButton: UIButton!
    
    var btnViewComments: UIButton!
    
    var xButtonImage: UIImageView!
    var checkButtonImage: UIImageView!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    // subcategory query
    var query = ""

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override init(frame: CGRect) {

        super.init(frame: frame)
        super.layoutSubviews()
        
        self.setupView()

        loadedCards = []

        
    }
    
    override func willMoveToSuperview(newSuperview: UIView?) {
        
        super.willMoveToSuperview(newSuperview)
        
//        self.fetchAndLoadCards()
    }

    func setupView() -> Void {
//        self.backgroundColor = UIColor(red: 0.92, green: 0.93, blue: 0.95, alpha: 1)
        self.backgroundColor = UIColor.whiteColor()
        
        // view comments button
        btnViewComments = UIButton(frame: CGRectMake(self.frame.size.width/2 - 80, self.frame.size.height/2 - CARD_HEIGHT/2 - 32 - 35, 161, 32))
        btnViewComments.layer.cornerRadius = 5
        btnViewComments.layer.borderColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0).CGColor
        btnViewComments.layer.borderWidth = 1
        btnViewComments.layer.masksToBounds = true
        btnViewComments.setTitle("View Comments (" + "\(0)" + ")", forState: UIControlState.Normal)
        btnViewComments.setTitleColor(UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0), forState: .Normal)
        btnViewComments.titleLabel?.font = UIFont(name: "Helvetica", size: 15)
        btnViewComments.addTarget(self, action: #selector(viewCommentTapped), forControlEvents: UIControlEvents.TouchUpInside)

        // x button
        xButton = UIButton(frame: CGRectMake((self.frame.size.width - CARD_WIDTH)/2 + 35, self.frame.size.height/2 + CARD_HEIGHT/2 - 25 + 10, 51, 51))
        xButton.addTarget(self, action: #selector(swipeLeft), forControlEvents: UIControlEvents.TouchUpInside)
        
        xButtonImage = UIImageView(frame: CGRectMake((self.frame.size.width - CARD_WIDTH)/2 + 35, self.frame.size.height/2 + CARD_HEIGHT/2 - 25 + 10, 51, 51))
        
        xButtonImage.image = UIImage(named: "xButton")
        xButtonImage.image = xButtonImage.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        xButtonImage.tintColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
        
        // check button
        checkButton = UIButton(frame: CGRectMake(self.frame.size.width/2 + CARD_WIDTH/2 - 85, self.frame.size.height/2 + CARD_HEIGHT/2 - 25 + 10, 51, 51))
        checkButton.addTarget(self, action: #selector(swipeRight), forControlEvents: UIControlEvents.TouchUpInside)
        
        checkButtonImage = UIImageView(frame: CGRectMake(self.frame.size.width/2 + CARD_WIDTH/2 - 85, self.frame.size.height/2 + CARD_HEIGHT/2 - 25 + 10, 51, 51))
        
        checkButtonImage.image = UIImage(named: "checkButton")
        checkButtonImage.image = checkButtonImage.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        checkButtonImage.tintColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
        
        self.addSubview(btnViewComments)
        self.addSubview(xButton)
        self.addSubview(xButtonImage)
        self.addSubview(checkButton)
        self.addSubview(checkButtonImage)
    }
    
    // Reset the card deck by removing all cards and links and fetching a new set of links
    func fetchAndLoadCards() {
    
        // start the login indicator
        appDelegate.showLoadingViewWithTitle(nil, sender: self)
        
        // clear all link/card holders
        removeAllCards()
        
        loadedCards.removeAll()
        homeList.removeAll()
        
        currentIdx = -1
        
        let user_id = UserDefault.getIntValue(R.pref_user_id)
        let token = UserDefault.getString(R.pref_user_token)
        
        if (self.query == "") {
        
            // Fetch recommended links without subcategory (query is empty string here).
            LinkAPI.getLinks(user_id, token: token, type: "recommended", offset: homeOffSet, length: R.OFFSET, sortCategory: "all"
                , sucess: { (message) in
                    
                    for index in 0 ..< message.count {
                        
                        self.homeList.append(message[index])
                    }
                    
                    self.loadCards()
                    
                    self.appDelegate.hideLoadingView()
                }
                , failure:  { (message) in
                    
                    self.appDelegate.hideLoadingView()
                    
                    JLToast.makeText(message, duration: 2.0).show()
            })
        } else {
            
            // Fetch recommended links with subcategory (query is such as worldnews, politics, etc).
            LinkAPI.getLinksWithQuery(user_id, token: token, type: "recommended", offset: homeOffSet, length: R.OFFSET, sortCategory: "all", query: self.query
                , sucess: { (message) in
                    
                    for index in 0 ..< message.count {
                        
                        self.homeList.append(message[index])
                    }
                    
                    self.loadCards()
                    
                    self.appDelegate.hideLoadingView()
                }
                , failure:  { (message) in
                    
                    self.appDelegate.hideLoadingView()
                    
                    JLToast.makeText(message, duration: 2.0).show()
            })
        }
    }
    
    func removeAllCards() {
        
        for card in loadedCards as [DraggableView] {
            
            card.removeFromSuperview()
        }
    }
    
    func createDraggableViewWithDataAtIndex(link: LinkModel) -> DraggableView {
        
        let draggableView = DraggableView(frame: CGRectMake((self.frame.size.width - CARD_WIDTH)/2, (self.frame.size.height - CARD_HEIGHT)/2 - 25 , CARD_WIDTH, CARD_HEIGHT))
        draggableView.delegate = self
        
        // set LinkModel to swipeview
        draggableView.setLinkModel(link)
        
        // Register the card for taps gestures.
        var tapGestureRecognizer: UITapGestureRecognizer!
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewDidTapped))
        tapGestureRecognizer.numberOfTapsRequired = 1
        draggableView.addGestureRecognizer(tapGestureRecognizer)
        
        return draggableView
    }
    
    func hasMoreLinks() -> Bool {
        
        if getRemainingCount() > 0 {
            
            return true
        }
            
        return false
    }
    
    func getRemainingCount() -> Int {
        
        return homeList.count - (currentIdx + 1)
    }
    
    // Returns the next link of the link collection. If there is no more links to return, returns nil.
    func getNextLink() -> LinkModel?{
        
        // Check if there is another link to return.
        if homeList.count > (currentIdx+1) {
            
            currentIdx += 1
            return homeList[currentIdx]
            
        } else{
            
            return nil
        }
    }

    func loadCards() -> Void {
        
        // Fill up the loadedCard list with links to its maximum buffer size.
        
        while hasMoreLinks() && loadedCards.count < MAX_LOADED_CARD_SIZE {
         
            let link = getNextLink()
            
            if (link != nil) {
                
                let newCard: DraggableView = self.createDraggableViewWithDataAtIndex(link!)
                loadedCards.append(newCard)
                
                // Add card's gui to the screen's queue (bottom of the deck)
                let newCardIndex = loadedCards.count - 1
                
                if (newCardIndex == 0) {
                    
                    self.addSubview(loadedCards[newCardIndex])
                    
                    self.btnViewComments.setTitle("View Comments (" + "\(link!.commentCnt)" + ")", forState: UIControlState.Normal)
                    
                } else {
                    
                    self.insertSubview(loadedCards[newCardIndex], belowSubview: loadedCards[newCardIndex - 1])
                }
                
            } else {
                
                break
            }
        }
        
        // Fetch more links from the server
        if homeList.count - currentIdx < LINKAPI_FETCH_SIZE_TRIGGER {
            
            homeOffSet += 1
            
            let user_id = UserDefault.getIntValue(R.pref_user_id)
            let token = UserDefault.getString(R.pref_user_token)
            
            if (self.query == "") {
                
                // Fetch recommended links without subcategory (query is empty string here)
                LinkAPI.getLinks(user_id, token: token, type: "recommended", offset: homeOffSet, length: R.OFFSET, sortCategory: "all"
                    , sucess: { (message) in
                        
                        for index in 0 ..< message.count {
                            
                            self.homeList.append(message[index])
                        }
                        
                        self.loadCards()
                    }
                    , failure:  { (message) in
                        
                        JLToast.makeText(message, duration: 2.0).show()
                })
                
            } else {
                
                // Fetch recommended links with subcategory (query is such as worldnews, politics, etc)
                LinkAPI.getLinksWithQuery(user_id, token: token, type: "recommended", offset: homeOffSet, length: R.OFFSET, sortCategory: "all", query: self.query
                    , sucess: { (message) in
                        
                        for index in 0 ..< message.count {
                            
                            self.homeList.append(message[index])
                        }
                        
                        self.loadCards()
                    }
                    , failure:  { (message) in
                        
                        JLToast.makeText(message, duration: 2.0).show()
                })
            }
        }
    }
    
    func viewDidTapped() {
        
        let user_id = UserDefault.getIntValue(R.pref_user_id)
        let token = UserDefault.getString(R.pref_user_token)
        
        let linkid = (loadedCards[0].linkModel as LinkModel).linkid
        
        appDelegate.showLoadingViewWithTitle(nil, sender: self)
        
        LinkAPI.setLinkAsViewed(user_id, token: token, linkid: linkid
            , success: { (statusCode, message) in
                
                self.appDelegate.hideLoadingView()
                
                JLToast.makeText(message, duration: 0.5).show()
                
                self.delegate.gotoWebView(self.loadedCards[0].linkModel as LinkModel)
                
            }
            , failure: { (statusCode, message) in
                
                self.appDelegate.hideLoadingView()
                
                JLToast.makeText(message, duration: 0.5).show()
        })
    }
    
    func viewCommentTapped() {
     
        delegate.gotoCommentView(loadedCards[0].linkModel as LinkModel)
    }

    func cardSwipedLeft(card: UIView) -> Void {
        
        let user_id = UserDefault.getIntValue(R.pref_user_id)
        let token = UserDefault.getString(R.pref_user_token)
        
        let linkid = (loadedCards[0].linkModel as LinkModel).linkid
        
        LinkAPI.updateUserLink(user_id, token: token, linkid: linkid, likevalue: -1
            , success: { (statusCode, message) in
                
                JLToast.makeText(message, duration: 0.5).show()
            }
            , failure: { (statusCode, message) in
                
                JLToast.makeText(message, duration: 0.5).show()
        })
        
        loadedCards.removeAtIndex(0)

        // Fill up the loadedCards buffer
        
        loadCards()
    }
    
    func cardSwipedRight(card: UIView) -> Void {
        
        let user_id = UserDefault.getIntValue(R.pref_user_id)
        let token = UserDefault.getString(R.pref_user_token)
        
        let linkid = (loadedCards[0].linkModel as LinkModel).linkid
        
        LinkAPI.updateUserLink(user_id, token: token, linkid: linkid, likevalue: 1
            , success: { (statusCode, message) in
                
                JLToast.makeText(message, duration: 0.5).show()
            }
            , failure: { (statusCode, message) in
                
                JLToast.makeText(message, duration: 0.5).show()
        })
        
        loadedCards.removeAtIndex(0)
        
        // Fill up the loadedCards buffer
        loadCards()
    }

    func swipeRight() -> Void {
        
        if loadedCards.count <= 0 {
            return
        }
        
        let dragView: DraggableView = loadedCards[0]
        dragView.overlayView.setMode(GGOverlayViewMode.GGOverlayViewModeRight)
        UIView.animateWithDuration(0.2, animations: {
            () -> Void in
            dragView.overlayView.alpha = 1
        })
        dragView.rightClickAction()
    }

    func swipeLeft() -> Void {
        if loadedCards.count <= 0 {
            return
        }
        let dragView: DraggableView = loadedCards[0]
        dragView.overlayView.setMode(GGOverlayViewMode.GGOverlayViewModeLeft)
        UIView.animateWithDuration(0.2, animations: {
            () -> Void in
            dragView.overlayView.alpha = 1
        })
        dragView.leftClickAction()
    }
}