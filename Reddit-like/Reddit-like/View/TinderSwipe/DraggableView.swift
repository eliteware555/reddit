//
//  DraggableView.swift
//  TinderSwipeCardsSwift
//
//  Created by Gao Chao on 4/30/15.
//  Copyright (c) 2015 gcweb. All rights reserved.
//

import Foundation
import UIKit
import JLToast

let ACTION_MARGIN: Float = 120      //%%% distance from center where the action applies. Higher = swipe further in order for the action to be called
let SCALE_STRENGTH: Float = 4       //%%% how quickly the card shrinks. Higher = slower shrinking
let SCALE_MAX:Float = 0.93          //%%% upper bar for how much the card shrinks. Higher = shrinks less
let ROTATION_MAX: Float = 1         //%%% the maximum rotation allowed in radians.  Higher = card can keep rotating longer
let ROTATION_STRENGTH: Float = 320  //%%% strength of rotation. Higher = weaker rotation
let ROTATION_ANGLE: Float = 3.14/8  //%%% Higher = stronger rotation angle

protocol DraggableViewDelegate {
    func cardSwipedLeft(card: UIView) -> Void
    func cardSwipedRight(card: UIView) -> Void
}

class DraggableView: UIView {
    
    var delegate: DraggableViewDelegate!
    var panGestureRecognizer: UIPanGestureRecognizer!
    var originPoint: CGPoint!
    var overlayView: OverlayView!

    var xFromCenter: Float!
    var yFromCenter: Float!
    
    var favoriteIcon: UILabel!
    var imageView: UIImageView!
    var sourceLabel: UILabel!
    var dateLabel: UILabel!
    var titleLabel: UILabel!
    var descriptionLabel: UILabel!
    
    var btnFavorite: UIButton!
    
    var linkModel: LinkModel!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        self.setupView()
        
        // imageview - link image
        imageView = UIImageView(frame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height))
        imageView.image = UIImage(named: "1.png")
        
        // date label
        dateLabel = UILabel(frame: CGRectMake(0, 0, 40, 20))
        dateLabel.text = "1M"
        dateLabel.numberOfLines = 1
        dateLabel.textAlignment = .Center
        dateLabel.textColor = UIColor.whiteColor()
        dateLabel.font = dateLabel.font.fontWithSize(14)
        dateLabel.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.75)
        
        // source label 
        sourceLabel = UILabel(frame: CGRectMake(dateLabel.frame.width, 0, self.frame.size.width - dateLabel.frame.width, 20))
        sourceLabel.text = "US News"
        sourceLabel.numberOfLines = 1
        sourceLabel.textAlignment = .Center
        sourceLabel.textColor = UIColor.whiteColor()
        sourceLabel.font = dateLabel.font.fontWithSize(14)
        sourceLabel.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.75)
        
        // title label
        titleLabel = UILabel(frame: CGRectMake(0, sourceLabel.frame.height, self.frame.size.width, 80))
        titleLabel.text = "no info given"
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = NSTextAlignment.Center
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.font = titleLabel.font.fontWithSize(19)
        titleLabel.backgroundColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0).colorWithAlphaComponent(0.5)
//        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        descriptionLabel = UILabel(frame: CGRectMake(0, frame.height - 100, self.frame.size.width, 100))
        descriptionLabel.text = "no info given"
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textAlignment = NSTextAlignment.Justified
        descriptionLabel.textColor = UIColor.whiteColor()
        descriptionLabel.font = descriptionLabel.font.fontWithSize(19)
        descriptionLabel.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.75)
//        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        
        // favorite icon
        favoriteIcon = UILabel(frame: CGRectMake (self.frame.size.width - 45 , 5, 40, 40))
        favoriteIcon.textAlignment = .Center
        favoriteIcon.textColor = UIColor.whiteColor()
        // awosome font
        favoriteIcon.font = UIFont.fontAwesomeOfSize(35)
        favoriteIcon.text = String.fontAwesomeIconWithCode("fa-heart-o")
        
        btnFavorite = UIButton(frame: CGRectMake (self.frame.size.width - 45, 0, 45, 45))
        btnFavorite.addTarget(self, action: #selector(favoriteTapped), forControlEvents: UIControlEvents.TouchUpInside)
        
        self.backgroundColor = UIColor.whiteColor()

        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(beingDragged(_:)))

        self.addGestureRecognizer(panGestureRecognizer)
        
        // add subview to super
        self.addSubview(imageView)
        self.addSubview(dateLabel)
        self.addSubview(sourceLabel)
        self.addSubview(titleLabel)
        self.addSubview(descriptionLabel)
        self.addSubview(favoriteIcon)
        self.addSubview(btnFavorite)
        
//        NSLayoutConstraint(item: titleLabel, attribute: .Leading, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: .Leading, multiplier: 1, constant:0).active = true
//        NSLayoutConstraint(item: titleLabel, attribute: .Trailing, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: .Trailing, multiplier: 1, constant:0).active = true
//        NSLayoutConstraint(item: titleLabel, attribute: .Top, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: .Top, multiplier: 1, constant:20).active = true
//        
//        NSLayoutConstraint(item: descriptionLabel, attribute: .Leading, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: .Leading, multiplier: 1, constant:0).active = true
//        NSLayoutConstraint(item: descriptionLabel, attribute: .Trailing, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: .Trailing, multiplier: 1, constant:0).active = true
//        NSLayoutConstraint(item: descriptionLabel, attribute: .Bottom, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: .Bottom, multiplier: 1, constant:0).active = true

        overlayView = OverlayView(frame: CGRectMake(self.frame.size.width/2-100, 0, 100, 100))
        overlayView.alpha = 0
        self.addSubview(overlayView)

        xFromCenter = 0
        yFromCenter = 0
    }

    func setupView() -> Void {
        
        self.layer.cornerRadius = 4;
        self.layer.shadowRadius = 3;
        self.layer.shadowOpacity = 0.4;
        self.layer.shadowOffset = CGSizeMake(1, 1);
    }
    
    func setLinkModel(model: LinkModel) {
        
        self.linkModel = model
        
        imageView.setImageWithUrl(NSURL(string: linkModel.imageURL)!, placeHolderImage: UIImage(named: "1.png"))
        
        // date & source
        dateLabel.text = linkModel.shortdate
        sourceLabel.text = linkModel.source
    
        // title 
        titleLabel.text = model.title
        
        // description
        descriptionLabel.text = model.subDetail
        
        if linkModel.isFavorite {
            
            let whiteAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            
            let str_heart = String.fontAwesomeIconWithCode("fa-heart")!
            let attributedText = NSMutableAttributedString(string:str_heart)
            
            attributedText.addAttributes(whiteAttributes, range: NSMakeRange(0, str_heart.characters.count))
            
            favoriteIcon.attributedText = attributedText
            
        } else {
            
            favoriteIcon.text = String.fontAwesomeIconWithCode("fa-heart-o")!
        }
    }

    func beingDragged(gestureRecognizer: UIPanGestureRecognizer) -> Void {
        xFromCenter = Float(gestureRecognizer.translationInView(self).x)
        yFromCenter = Float(gestureRecognizer.translationInView(self).y)
        
        switch gestureRecognizer.state {
        case UIGestureRecognizerState.Began:
            self.originPoint = self.center
        case UIGestureRecognizerState.Changed:
            let rotationStrength: Float = min(xFromCenter/ROTATION_STRENGTH, ROTATION_MAX)
            let rotationAngle = ROTATION_ANGLE * rotationStrength
            let scale = max(1 - fabsf(rotationStrength) / SCALE_STRENGTH, SCALE_MAX)

            self.center = CGPointMake(self.originPoint.x + CGFloat(xFromCenter), self.originPoint.y + CGFloat(yFromCenter))

            let transform = CGAffineTransformMakeRotation(CGFloat(rotationAngle))
            let scaleTransform = CGAffineTransformScale(transform, CGFloat(scale), CGFloat(scale))
            self.transform = scaleTransform
            self.updateOverlay(CGFloat(xFromCenter))
        case UIGestureRecognizerState.Ended:
            self.afterSwipeAction()
        case UIGestureRecognizerState.Possible:
            fallthrough
        case UIGestureRecognizerState.Cancelled:
            fallthrough
        case UIGestureRecognizerState.Failed:
            fallthrough
        default:
            break
        }
    }

    func updateOverlay(distance: CGFloat) -> Void {
        if distance > 0 {
            overlayView.setMode(GGOverlayViewMode.GGOverlayViewModeRight)
        } else {
            overlayView.setMode(GGOverlayViewMode.GGOverlayViewModeLeft)
        }
        overlayView.alpha = CGFloat(min(fabsf(Float(distance))/100, 0.4))
    }

    func afterSwipeAction() -> Void {
        let floatXFromCenter = Float(xFromCenter)
        if floatXFromCenter > ACTION_MARGIN {
            self.rightAction()
        } else if floatXFromCenter < -ACTION_MARGIN {
            self.leftAction()
        } else {
            UIView.animateWithDuration(0.3, animations: {() -> Void in
                self.center = self.originPoint
                self.transform = CGAffineTransformMakeRotation(0)
                self.overlayView.alpha = 0
            })
        }
    }
    
    func rightAction() -> Void {
        let finishPoint: CGPoint = CGPointMake(500, 2 * CGFloat(yFromCenter) + self.originPoint.y)
        UIView.animateWithDuration(0.3,
            animations: {
                self.center = finishPoint
            }, completion: {
                (value: Bool) in
                self.removeFromSuperview()
        })
        delegate.cardSwipedRight(self)
    }

    func leftAction() -> Void {
        let finishPoint: CGPoint = CGPointMake(-500, 2 * CGFloat(yFromCenter) + self.originPoint.y)
        UIView.animateWithDuration(0.3,
            animations: {
                self.center = finishPoint
            }, completion: {
                (value: Bool) in
                self.removeFromSuperview()
        })
        delegate.cardSwipedLeft(self)
    }

    func rightClickAction() -> Void {
        let finishPoint = CGPointMake(600, self.center.y)
        UIView.animateWithDuration(0.3,
            animations: {
                self.center = finishPoint
                self.transform = CGAffineTransformMakeRotation(1)
            }, completion: {
                (value: Bool) in
                self.removeFromSuperview()
        })
        delegate.cardSwipedRight(self)
    }

    func leftClickAction() -> Void {
        let finishPoint: CGPoint = CGPointMake(-600, self.center.y)
        UIView.animateWithDuration(0.3,
            animations: {
                self.center = finishPoint
                self.transform = CGAffineTransformMakeRotation(1)
            }, completion: {
                (value: Bool) in
                self.removeFromSuperview()
        })
        delegate.cardSwipedLeft(self)
    }
    
    func updateFavoriteIcon() {
        
        if (linkModel.isFavorite) {
            
            favoriteIcon.text = String.fontAwesomeIconWithCode("fa-heart")!
            
        } else {
            
            favoriteIcon.text = String.fontAwesomeIconWithCode("fa-heart-o")!
        }
    }
    
    func favoriteTapped() {
        
        appDelegate.showLoadingViewWithTitle(nil, sender: self)
        
        let isFavorite = !linkModel.isFavorite
        
        let user_id = UserDefault.getIntValue(R.pref_user_id)
        let token = UserDefault.getString(R.pref_user_token)
        
        let linkid = linkModel.linkid
        
        LinkAPI.setUserLinkFavorite(user_id, token: token, linkid: linkid, isFavorite: isFavorite
            , success: { (statusCode, message) in
                
                JLToast.makeText(message, duration: 0.5).show()
                
                self.appDelegate.hideLoadingView()
                
                self.linkModel.isFavorite = isFavorite
                
                self.updateFavoriteIcon()
                
            }
            , failure:  { (statusCode, message) in
                
                JLToast.makeText(message, duration: 0.5).show()
                
                self.appDelegate.hideLoadingView()
        })
    }
}
