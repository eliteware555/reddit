//
//  SubTopicCell.swift
//  Reddit-like
//
//  Created by Victory on 28/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit

class SubTopicCell: UITableViewCell {
    
    @IBOutlet weak var lblSubTopicItem: UILabel!
    @IBOutlet weak var checkbox: M13Checkbox!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        self.separatorInset = UIEdgeInsetsMake(0, 16, 0, 16)
        checkbox.contentHorizontalAlignment = .Center
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
