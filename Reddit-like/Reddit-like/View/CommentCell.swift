//
//  CommentCell.swift
//  Reddit-like
//
//  Created by Victory on 06/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet private weak var imvImage: UIImageView!
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var lblCommentCount: UILabel!
    
    @IBOutlet weak var lblBody: UILabel!

    @IBOutlet weak var leftofImage: NSLayoutConstraint!
    
    @IBOutlet weak var lblExpandMark: UILabel!
    
    @IBOutlet weak var imvBody: UIImageView!
    
    var bodyTapActionBlock : (CommentCell -> Void)?;
    
    override func awakeFromNib() {
        
        selectedBackgroundView? = UIView.init()
        selectedBackgroundView?.backgroundColor = UIColor.clearColor()
        
        let bodyTapRec = UITapGestureRecognizer(target: self, action: #selector(bodyTapped(_:)))
        imvBody.userInteractionEnabled = true
        imvBody.addGestureRecognizer(bodyTapRec)
    }
    
    func setupModel(withTitle user_image: UIImage, title: String, comment_count: String) {
        
        imvImage.image = user_image
        lblTitle.text = title
        lblCommentCount.text = comment_count
    }
    
    func setModel(model: CommentModel, level: Int) {

        lblTitle.text = model.user_name
        lblCommentCount.text = model.comment_score + " points " + "• " + model.comment_shortdate

        if (model.replies.count == 0) {
            
            lblExpandMark.hidden = true
            
        } else {
            
            lblExpandMark.hidden = false
        }
        
        let left = 20.0 + 20.0 * CGFloat(level)
        
        self.backgroundColor = UIColor.clearColor()
        
        leftofImage.constant = left
        
        imvImage.setImageWithUrl(NSURL(string: model.user_image_url)!, placeHolderImage: UIImage(named: "8.png"))
        
        setBody("")
    }
    
    func setBody(body: String) {
        
        if (body == "") {
            
            self.lblExpandMark.text = "+"
            
        } else {
            
            self.lblExpandMark.text = "-"
        }
        
        lblBody.text = body
    }
    
    func bodyTapped(sender: AnyObject) {
        
        if let action = bodyTapActionBlock {
            
            action(self)
        }
    }
    
    func titleTapped(sender: AnyObject) {

//        if let action  = titleTapActionBlock {
//            
//            action(self)
//        }
    }
}
