//
//  MainCell.swift
//  Reddit-like
//
//  Created by Victory on 04/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit
import FontAwesome

class LinkCell: UITableViewCell {
    
    @IBOutlet weak var imvItem: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSource: UILabel!

    
    @IBOutlet weak var lblViewComments: UILabel!
    @IBOutlet weak var btnViewComments: UIButton!
    
    @IBOutlet weak var lblShortDate: UILabel!
    
    @IBOutlet weak var btnCircleCheck: UIButton!
    @IBOutlet weak var btnCircleX: UIButton!
    
    @IBOutlet weak var imvCircleCheck: UIImageView!
    @IBOutlet weak var imvCircleX: UIImageView!
    
    @IBOutlet weak var btnFavorite: UIButton!
    
    @IBOutlet weak var lblFavorite: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.separatorInset = UIEdgeInsetsMake(0, 8, 0, 8);
        
        btnViewComments.layer.borderColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0).CGColor
        
        imvCircleCheck.image = imvCircleCheck.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        imvCircleCheck.tintColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
        
        imvCircleX.image = imvCircleX.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        imvCircleX.tintColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setMainModel(model: LinkModel) {
        
//        imvItem.image = model.image
        lblTitle.text = model.title
        lblSource.text = model.source
        
        lblShortDate.text = model.shortdate
        
        lblViewComments.text = "View comments (" + "\(model.commentCnt)" + ")"
        
        lblFavorite.font = UIFont.fontAwesomeOfSize(20)

        if (model.isFavorite) {
            
            let pinkBoldAttributes = [NSForegroundColorAttributeName: UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)]
            
            let str_heart = String.fontAwesomeIconWithCode("fa-heart")!
            let attributedText = NSMutableAttributedString(string:str_heart)
            
            attributedText.addAttributes(pinkBoldAttributes, range: NSMakeRange(0, str_heart.characters.count))
            
            lblFavorite.attributedText = attributedText
            
        } else {
            
            lblFavorite.text = String.fontAwesomeIconWithCode("fa-heart-o")!
        }

        
        imvItem.setImageWithUrl(NSURL(string: model.imageURL)!, placeHolderImage: UIImage(named: "1.png"))
    }
}


