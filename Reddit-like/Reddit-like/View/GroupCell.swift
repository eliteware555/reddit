//
//  GroupCell.swift
//  Reddit-like
//
//  Created by Victory on 04/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit

class GroupCell: UITableViewCell {
    
    @IBOutlet weak var imvLIcon: UIImageView!
    @IBOutlet weak var imvRIcon: UIImageView!
    @IBOutlet weak var lblMenuItem: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
