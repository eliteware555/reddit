//
//  SignupViewController.swift
//  Reddit-like
//
//  Created by Victory on 06/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit
import JLToast

class SignupViewController: UIViewController, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    // profile image
    @IBOutlet weak var imvProfile: UIImageView!
    
    // UIImagePickerController
    var picker: UIImagePickerController? = UIImagePickerController()
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    //
    @IBOutlet weak var txtNameField: UITextField!
    @IBOutlet weak var txtEmailField: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtBirthday: UITextField!
    
    @IBOutlet weak var imvGenderIcon: UIImageView!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    
    var strPhotoPath: String = ""
    
    var strGender = "m"
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        initView()
    }
    
    func initView() {
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        // customize text field
        
        // user name input field
        let leftNameImageView = UIImageView(frame: CGRectMake(0, 0, 40, 36))
        leftNameImageView.image = UIImage(named: "ic_name")
        leftNameImageView.contentMode = .Center
        
        // only left side corner radius
        let rectShape = CAShapeLayer()
        rectShape.bounds = leftNameImageView.frame
        rectShape.position = leftNameImageView.center
        rectShape.path = UIBezierPath(roundedRect: leftNameImageView.bounds, byRoundingCorners: [.BottomLeft, .TopLeft], cornerRadii: CGSize(width: 5, height: 5)).CGPath
        
        leftNameImageView.layer.backgroundColor = UIColor(red: 216/255.0, green: 67/255.0, blue: 21/255.0, alpha: 0.8).CGColor
        leftNameImageView.layer.mask = rectShape
        
        let leftNameView = UIView(frame: CGRectMake(0, 0, 46, 36))
        leftNameView.addSubview(leftNameImageView)
        
        txtNameField.leftView = leftNameView
        txtNameField.leftViewMode = UITextFieldViewMode.Always
        
        // email address field
        let leftEmailImageView = UIImageView(frame: CGRectMake(0, 0, 40, 36))
        leftEmailImageView.image = UIImage(named: "ic_mail")
        leftEmailImageView.contentMode = .Center
        
        leftEmailImageView.layer.backgroundColor = UIColor(red: 216/255.0, green: 67/255.0, blue: 21/255.0, alpha: 1.0).CGColor
        leftEmailImageView.layer.mask = rectShape
        
        let leftEmailView = UIView(frame: CGRectMake(0, 0, 46, 36))
        leftEmailView.addSubview(leftEmailImageView)
        
        txtEmailField.leftView = leftEmailView
        txtEmailField.leftViewMode = UITextFieldViewMode.Always
        
        // Password field
        let leftPwdImageView = UIImageView(frame: CGRectMake(0, 0, 40, 36))
        leftPwdImageView.image = UIImage(named: "ic_lock")
        leftPwdImageView.contentMode = .Center
        
        leftPwdImageView.layer.backgroundColor = UIColor(red: 216/255.0, green: 67/255.0, blue: 21/255.0, alpha: 1.0).CGColor
        leftPwdImageView.layer.mask = rectShape
        
        let leftPwdView = UIView(frame: CGRectMake(0, 0, 46, 36))
        leftPwdView.addSubview(leftPwdImageView)
        
        txtPassword.leftView = leftPwdView
        txtPassword.leftViewMode = UITextFieldViewMode.Always
        
        // Birthday field
        let leftBirthImageView = UIImageView(frame: CGRectMake(0, 0, 40, 36))
        leftBirthImageView.image = UIImage(named: "ic_birthday")
        leftBirthImageView.contentMode = .Center
        
        leftBirthImageView.layer.backgroundColor = UIColor(red: 216/255.0, green: 67/255.0, blue: 21/255.0, alpha: 1.0).CGColor
        leftBirthImageView.layer.mask = rectShape
        
        let leftBirthday = UIView(frame: CGRectMake(0, 0, 46, 36))
        leftBirthday.addSubview(leftBirthImageView)
        
        txtBirthday.leftView = leftBirthday
        txtBirthday.leftViewMode = UITextFieldViewMode.Always
        
        // gender selection icon
        imvGenderIcon.layer.backgroundColor = UIColor(red: 216/255.0, green: 67/255.0, blue: 21/255.0, alpha: 1.0).CGColor
        imvGenderIcon.layer.mask = rectShape
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(animated)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func genderSelectionTapped(sender: UIButton) {
        
        if (sender == btnMale) {
            
            // male selected
            selectGender(0)
            
            strGender = "m"
            
        } else {
            
            // female selected
            selectGender(1)
            
            strGender = "f"
        }
    }
    
    func selectGender(gender: Int) {
        
        if (gender == 0) {
            
            btnMale.backgroundColor = UIColor(red: 66/255.0, green: 66/255.0, blue: 66/255.0, alpha: 0.7)
            btnFemale.layer.backgroundColor = UIColor.clearColor().CGColor
            
        } else {
            
//            btnFemale.layer.backgroundColor = UIColor(red: 66/255.0, green: 66/255.0, blue: 66/255.0, alpha: 0.7).CGColor
            btnFemale.backgroundColor = UIColor(red: 66/255.0, green: 66/255.0, blue: 66/255.0, alpha: 0.7)
            btnMale.backgroundColor = UIColor.clearColor()
        }
    }
    
    // MARK: UITextFieldDelegate
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if (textField == txtBirthday) {
            
//            let date: NSDate = NSDate()
//            let dateFormatter:NSDateFormatter = NSDateFormatter()
//            dateFormatter.dateFormat = "yyyy-MM-dd"
//            let dateString: String = dateFormatter.stringFromDate(date)
//            
//            textField.text = dateString
            
            let datePickerView: UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = UIDatePickerMode.Date
            textField.inputView = datePickerView
            
            datePickerView.addTarget(self, action: #selector(datePickerValueChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
        }
    }
    
    func datePickerValueChanged(sender: UIDatePicker) {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        txtBirthday.text = dateFormatter.stringFromDate(sender.date)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if (textField == txtNameField) {
            
            txtEmailField.becomeFirstResponder()
            
        } else if (txtEmailField == textField) {
            
            txtPassword.becomeFirstResponder()
            
        } else  if (txtPassword == textField) {
            
            txtBirthday.becomeFirstResponder()
        }
        
        textField.resignFirstResponder()
        
        return true
    }
    
    // profile image tap
    @IBAction func profileImageTapped(sender: AnyObject) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .Default) { (alert: UIAlertAction) -> Void in
            self.openCamera()
        }
        let albumAction = UIAlertAction(title: "Gallery", style: .Default) { (alert: UIAlertAction!) -> Void in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (alert: UIAlertAction!) -> Void in
            
        }
        
        picker?.delegate = self
        
        alert.addAction(cameraAction)
        alert.addAction(albumAction)
        alert.addAction(cancelAction)
        presentViewController(alert, animated: true, completion: nil)
    }
    
    // open gallery
    func openGallery() {
        picker?.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        picker?.allowsEditing = true
        self.presentViewController(picker!, animated: true, completion: nil)
    }
    
    // open camera
    func openCamera() {
        
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
            
            picker?.sourceType = UIImagePickerControllerSourceType.Camera
            picker?.allowsEditing = true
            picker?.modalPresentationStyle = .FullScreen
            self.presentViewController(picker!, animated: true, completion: nil)
        }
    }
  
    // MARK: - UIImagePickerController Delegate
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let newImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            
            strPhotoPath = CommonUtils.saveToFile(newImage)
            
            imvProfile.contentMode = .ScaleAspectFit
            imvProfile.image = UIImage(contentsOfFile: strPhotoPath)

        }
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // check valid
    func isValid() -> Bool {
        
        if txtNameField.text!.isEmpty {
            
            appDelegate.showAlertDialog(nil, message: R.required_name_field, positive: R.alert_ok, negative: nil, sender: self)
            
            return false
            
        } else if (txtNameField.text?.characters.count < 4 || txtNameField.text?.characters.count > 16 || !CommonUtils.isAlphNumberic(txtNameField.text!)) {
            
            appDelegate.showAlertDialog(nil, message: R.input_correct_name, positive: R.alert_ok, negative: nil, sender: self)
            
            return false
            
        } else if txtEmailField.text!.isEmpty {
            
            appDelegate.showAlertDialog(nil, message: R.input_your_email, positive: R.alert_ok, negative: nil, sender: self)
            return false
            
        } else if (!CommonUtils.isValidEmail(txtEmailField.text!)) {
            
            appDelegate.showAlertDialog(nil, message: R.input_correct_email, positive:R.alert_ok, negative: nil, sender: self);
            return false
            
        } else if txtPassword.text!.isEmpty {
            
            appDelegate.showAlertDialog(nil, message: R.input_password, positive: R.alert_ok, negative: nil, sender: self)
            return false
            
        } else if txtPassword.text!.characters.count < 8 || txtPassword.text?.characters.count > 16 {
            
            appDelegate.showAlertDialog(nil, message: R.password_length, positive: R.alert_ok, negative: nil, sender: self)
            
            return false
            
        } else if (txtBirthday.text!.isEmpty) {
            
            appDelegate.showAlertDialog(nil, message: R.input_birthday, positive: R.alert_ok, negative: nil, sender: self)
            
            return false
            
        } else if (strPhotoPath == "") {
            
            appDelegate.showAlertDialog(nil, message: R.add_photo, positive: R.alert_ok, negative: nil, sender: self)
            
            return false
            
        }
        
        return true
    }
    
    // sign up button action
    @IBAction func signupTapped(sender: AnyObject) {
        
        self.view.endEditing(true)
        
//        gotoTopics()
        
        if (isValid()) {
        
            let userName = txtNameField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let userEmail = txtEmailField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let password = txtPassword.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let birthday = txtBirthday.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
            let _lat = appDelegate.userLat
            let _lon = appDelegate.userLon
        
            let device = CommonUtils.escape(CommonUtils.getDeviceType())
            
            let os = CommonUtils.escape(CommonUtils.getSystemVersion())
            
            let appversion = CommonUtils.escape(CommonUtils.getAppVersion())
            
            print("device type: " + device)
            print("ios version: " + os)
            print("app version: " + appversion)
            
            appDelegate.showLoadingViewWithTitle(nil, sender: self)
            
            LinkAPI.createUserWithLatAndLong(userName, str_useremail: userEmail, password: password, birthday: birthday, gender: strGender, lat: _lat, long: _lon, device: device, os: os, appversion: appversion
                , success: { (statusCode, message) in
                    
                    JLToast.makeText(message, duration: 1.0).show()
                    
                    // save user credentials
                    UserDefault.setString(userName, key: R.pref_user_name)
                    UserDefault.setString(userEmail, key: R.pref_user_email)
                    UserDefault.setString(password, key: R.pref_user_password)
                    
                    UserDefault.setString(self.strGender, key: R.pref_user_gender)
                    UserDefault.setString(birthday, key: R.pref_user_birthday)
                    
                    // get user token
                    self.getToken(userName, password: password, email: userEmail)
                    
                }, failure: { (statusCode, message) in
                    
                    self.appDelegate.hideLoadingView()
                    
                    JLToast.makeText(message, duration: 2.0).show()
            })
        }     
    }
    
    // get token
    func getToken(name: String!, password: String!, email: String!) {
        
        LinkAPI.getToken(name, password: password, email: email
            , success: { (statusCode, message, user_id, token, tokenExpiration) in
                
                // save user info
                UserDefault.setIntValue(user_id, key: R.pref_user_id)
                UserDefault.setString(token, key: R.pref_user_token)
                UserDefault.setString(tokenExpiration, key: R.pref_expiration)
                
                JLToast.makeText(message, duration: 1.0).show()
                
                // verify token
                self.verifyToken(user_id, token: token)
                
            }, failure:  { (statusCode, message) in
                
                self.appDelegate.hideLoadingView()
                
                JLToast.makeText(message, duration: 2.0).show()
        })
    }
    
    // verify token
    func verifyToken(user_id: Int!, token: String!) {
        
        LinkAPI.verifyToken(user_id, token: token
            , success: { (statusCode, message) in
                
                JLToast.makeText(message, duration: 1.0).show()
                
                self.uploadImage(user_id, token: token)
            
            }, failure: { (statusCode, message) in
                
                self.appDelegate.hideLoadingView()
                
                JLToast.makeText(message, duration: 2.0).show()
                
        })
    }
    
    // upload image
    func uploadImage(user_id: Int!, token: String!) {
        
        LinkAPI.addUserImage(user_id, token: token, image_url: strPhotoPath
            , success: { (statusCode, message) in
                
//                self.appDelegate.hideLoadingView()
                
                JLToast.makeText(message, duration: 2.0).show()
                
                self.getUserImage(user_id, token: token)
                
            }, failure:  { (statusCode, message) in
                
                self.appDelegate.hideLoadingView()
                
                self.gotoTopics()
                
                JLToast.makeText(message, duration: 2.0).show()
        })
    }
    
    func getUserImage(user_id: Int!, token: String!) {
        
        LinkAPI.getUserImage(user_id, token: token
            , success: { (statusCode, message) in
                
                self.appDelegate.hideLoadingView()
                
                JLToast.makeText(message, duration: 2.0).show()
                
                self.gotoTopics()
                
            }, failure:  { (statusCode, message) in
                
                self.appDelegate.hideLoadingView()
                
                self.gotoTopics()
                
                JLToast.makeText(message, duration: 2.0).show()
        })
    }
    
    func gotoTopics() {
        
        self.performSegueWithIdentifier("SegueSing2Topics", sender: self)
    }
    
    // redirect to main linked view
    func gotoMainView() {
        
        UserDefault.setBoolValue(true, key: R.pref_auto_login)
        
        let revealVC = self.storyboard?.instantiateViewControllerWithIdentifier("SWReveal") as! SWRevealViewController
        
        UIApplication.sharedApplication().keyWindow?.rootViewController = revealVC
    }
}
