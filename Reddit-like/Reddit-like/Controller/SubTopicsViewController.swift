//
//  SubTopicsViewController.swift
//  Reddit-like
//
//  Created by Victory on 28/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit

class SubTopicsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var mainTopicIdx = 0
    
    var subTopics = [TopicModel]()
    
    @IBOutlet weak var subTopicList: UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        initView()
    }
    
    func initView() {
        
        navigationItem.title = CommonUtils.getTopics()[mainTopicIdx]
        
        subTopicList.tableFooterView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
    }
    
    // MARK: - UITableView DataSource & Delegate
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return subTopics.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("SubTopicCell") as! SubTopicCell
        
        cell.lblSubTopicItem.text = subTopics[indexPath.row].item
        
        cell.checkbox.tag = 300 + indexPath.row
        
        if (subTopics[indexPath.row].selected) {
            
            cell.checkbox.setCheckState(.Checked, animated: true)
            
        } else {
            
            cell.checkbox.setCheckState(.Unchecked, animated: true)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 60
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! SubTopicCell
        
        cell.checkbox.toggleCheckState(true)
        
        subTopics[indexPath.row].selected = !subTopics[indexPath.row].selected
    }
    
    @IBAction func topicCheck(sender: M13Checkbox) {
        
        let subTopicIdx = sender.tag - 300
        
        subTopics[subTopicIdx].selected = !subTopics[subTopicIdx].selected
    }
}



