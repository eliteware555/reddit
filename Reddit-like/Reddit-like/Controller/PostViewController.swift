//
//  PostViewController.swift
//  Reddit-like
//
//  Created by Victory on 08/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit
import JLToast

class PostViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var lblRemainCharactors: UILabel!
    
    @IBOutlet weak var imvUserImage: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblCommentbody: UILabel!
    
    @IBOutlet weak var txvCommentField: UITextView!
    
    var isReply: Bool = false
    var replyModel: CommentModel!
    
    var linkModel: LinkModel!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }
    
    func initView() {
        
        if (isReply) {

            imvUserImage.setImageWithUrl(NSURL(string: replyModel.user_image_url)!, placeHolderImage: UIImage(named: "3.png"))
            lblUserName.text = replyModel.user_name
            lblCommentbody.text = replyModel.comment_body
        } else {
            
            imvUserImage.hidden = true
            lblUserName.hidden = true
            lblCommentbody.hidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textViewDidChange(textView: UITextView) {
        
        lblRemainCharactors.text = "\(120 - textView.text.characters.count) Charactoers Remaining"
        
        if (textView.text.characters.count == 120) {
            self.view.endEditing(true)
        }
    }    
    
    @IBAction func closeTapped(sender: AnyObject) {
        
        // end editing
        self.view.endEditing(true)
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func replyTapped(sender: AnyObject) {
        
        // end editing
        self.view.endEditing(true)
        
        let commentBody = txvCommentField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        if (commentBody.characters.count == 0) {
            
            JLToast.makeText(R.input_comment_body, duration: 1.0).show()
            
            return
        }
        
        appDelegate.showLoadingViewWithTitle(nil, sender: self)
        
        let user_id = UserDefault.getIntValue(R.pref_user_id)
        let token = UserDefault.getString(R.pref_user_token)
        
        let linkid = linkModel.linkid
        
        let replyToCommentid = isReply ? "\(replyModel.commentId)" : ""
        
        LinkAPI.postComment(user_id, token: token, linkid: linkid, replyToCommentid: replyToCommentid, body: commentBody
            , success: { (message) in
            
                self.appDelegate.hideLoadingView()
                
                JLToast.makeText(message, duration: 2.0).show()
                
                self.navigationController?.popViewControllerAnimated(true)
            
            }
            , failure:  { (message) in
               
                self.appDelegate.hideLoadingView()
                
                JLToast.makeText(message, duration: 2.0).show()
                
        })
        
        // for test
//        self.navigationController?.popViewControllerAnimated(true)
    }
}
