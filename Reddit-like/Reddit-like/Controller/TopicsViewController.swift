//
//  TopicsViewController.swift
//  Reddit-like
//
//  Created by Victory on 28/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit
import JLToast

class TopicsViewController: UIViewController {
    
    var selectedTopicIdx = 0
    
    var topics: [TopicModel] = []
    
    @IBOutlet weak var usCircle: UIImageView!
    @IBOutlet weak var usIcon: UIImageView!
    @IBOutlet weak var usItem: UILabel!
    
    @IBOutlet weak var worldCircle: UIImageView!
    @IBOutlet weak var worldIcon: UIImageView!
    @IBOutlet weak var worldItem: UILabel!
    
    @IBOutlet weak var sportsCircle: UIImageView!
    @IBOutlet weak var sportsIcon: UIImageView!
    @IBOutlet weak var sportsItem: UILabel!
    
    @IBOutlet weak var businessCircle: UIImageView!
    @IBOutlet weak var businessIcon: UIImageView!
    @IBOutlet weak var businessItem: UILabel!
    
    @IBOutlet weak var scienceCircle: UIImageView!
    @IBOutlet weak var scienceIcon: UIImageView!
    @IBOutlet weak var scienceItem: UILabel!
    
    @IBOutlet weak var techCircle: UIImageView!
    @IBOutlet weak var techIcon: UIImageView!
    @IBOutlet weak var techItem: UILabel!
    
    @IBOutlet weak var entertainmentCircle: UIImageView!
    @IBOutlet weak var entertainmentIcon: UIImageView!
    @IBOutlet weak var entertainmentItem: UILabel!
    
    @IBOutlet weak var artsCircle: UIImageView!
    @IBOutlet weak var artsIcon: UIImageView!
    @IBOutlet weak var artsItem: UILabel!
    
    @IBOutlet weak var lifestyleCircle: UIImageView!
    @IBOutlet weak var lifestyleIcon: UIImageView!
    @IBOutlet weak var lifestyleItem: UILabel!
    
    var topic_circles: [UIImageView] = []
    var topic_icons: [UIImageView] = []
    var topic_items: [UILabel] = []
    
    var isFirstLoading = true
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        initVars()
        
        initView()
    }
    
    func initVars() {
        
        for topic_index in 0 ..< CommonUtils.getTopics().count {
            
            var sub_topics: [TopicModel] = []
            
            for sub_index in 0 ..< CommonUtils.getSubTopics(topic_index).count {
                
                sub_topics.append(TopicModel(selected: false, item: CommonUtils.getSubTopics(topic_index)[sub_index]))
            }
            
            topics.append(TopicModel(selected: false, item: CommonUtils.getTopics()[topic_index], subTopics: sub_topics))
        }
        
        topic_circles.append(usCircle)
        topic_circles.append(worldCircle)
        topic_circles.append(sportsCircle)
        topic_circles.append(businessCircle)
        topic_circles.append(scienceCircle)
        topic_circles.append(techCircle)
        topic_circles.append(entertainmentCircle)
        topic_circles.append(artsCircle)
        topic_circles.append(lifestyleCircle)
        
        topic_icons.append(usIcon)
        topic_icons.append(worldIcon)
        topic_icons.append(sportsIcon)
        topic_icons.append(businessIcon)
        topic_icons.append(scienceIcon)
        topic_icons.append(techIcon)
        topic_icons.append(entertainmentIcon)
        topic_icons.append(artsIcon)
        topic_icons.append(lifestyleIcon)
        
        topic_items.append(usItem)
        topic_items.append(worldItem)
        topic_items.append(sportsItem)
        topic_items.append(businessItem)
        topic_items.append(scienceItem)
        topic_items.append(techItem)
        topic_items.append(entertainmentItem)
        topic_items.append(artsItem)
        topic_items.append(lifestyleItem)
    }
    
    func initView() {
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if (!isFirstLoading) {
        
            updateTopicSelection(selectedTopicIdx) // selectedTopicIdx
            
        } else {
        
            isFirstLoading = false
        }
    }
    
    func updateTopicSelection(topic_index: Int) {
        
        // check subTopics selection state..
        
        let topicSelected = topics[topic_index].selected
        
        for sub_index in 0 ..< topics[topic_index].subTopics.count {
                
            if (topics[topic_index].subTopics[sub_index].selected) {
                
                topics[topic_index].selected = true
                break
            }
        }
        
        // there is no change in topic selected state
        if (topicSelected == topics[topic_index].selected) {
            
            return
        }
        
        if (topics[topic_index].selected) {
            
            topic_circles[topic_index].image = UIImage(named: "ring_fill")
            
            topic_icons[topic_index].image = topic_icons[topic_index].image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            topic_icons[topic_index].tintColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
            topic_items[topic_index].textColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
            
        } else {
            
            topic_circles[topic_index].image = UIImage(named: "ring")
            
            topic_icons[topic_index].image = topic_icons[topic_index].image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            topic_icons[topic_index].tintColor = UIColor.whiteColor()
            topic_items[topic_index].textColor = UIColor.whiteColor()
        }
    }
    
    @IBAction func topicTapped(sender: UIButton) {
        
        selectedTopicIdx = sender.tag - 200
        
        self.performSegueWithIdentifier("SegueTopics2SubTopics", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "SegueTopics2SubTopics" {
            
            let destVC = segue.destinationViewController as! SubTopicsViewController
            
            destVC.subTopics = topics[selectedTopicIdx].subTopics
            
            destVC.mainTopicIdx = selectedTopicIdx
        }
    }
    
    @IBAction func doneTapped(sender: UIButton) {
        
        if (!checkValid()) {
            
            appDelegate.showAlertDialog(nil, message: "Please select at least three topics.", positive: R.alert_ok, negative: nil, sender: self)
            
            return
        }
        
        // update top
        var selectedTopics = [TopicModel]()
        
        let user_id = UserDefault.getIntValue(R.pref_user_id)!
        let user_token = UserDefault.getString(R.pref_user_token)!
        
        for topic_index in 0 ..< topics.count {
            
            for sub_index in 0 ..< topics[topic_index].subTopics.count {
                
                if (topics[topic_index].subTopics[sub_index].selected) {
                    
                    selectedTopics.append(topics[topic_index].subTopics[sub_index])
                }
            }
        }
        
        appDelegate.showLoadingViewWithTitle(nil, sender: selectedTopics)
        
        LinkAPI.updateUserTopics(user_id, token: user_token, topics: selectedTopics
            , success: { (message) in
                
                JLToast.makeText(message, duration: 1.0).show()
                
                self.appDelegate.hideLoadingView()
                
                self.showSuccessMessage()
            
            }) { (message) in
                
                self.appDelegate.hideLoadingView()
                
                JLToast.makeText(message, duration: 2.0).show()
        }
    }
    
    func checkValid() -> Bool {
        
        var selectedTopicCount = 0
        
        for index in 0 ..< topics.count {
            
            if (topics[index].selected) {
                
                selectedTopicCount += 1
            }
        }
        
        if (selectedTopicCount < 3) {
            
            return false
        }
        
        return true
    }
    
    // show success message
    func showSuccessMessage() {
        
        let alert = UIAlertController(title: "Activation", message: "Please click the emailed activation link. Accounts not activated in 24 hours will be deleted", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: R.alert_ok, style: UIAlertActionStyle.Default
            , handler:{ (UIAlertAction)in
                
                self.gotoMainView()
            
            }))
        
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    // redirect to main linked view
    func gotoMainView() {
        
        UserDefault.setBoolValue(true, key: R.pref_auto_login)
        
        let revealVC = self.storyboard?.instantiateViewControllerWithIdentifier("SWReveal") as! SWRevealViewController
        
        UIApplication.sharedApplication().keyWindow?.rootViewController = revealVC
    }
}
