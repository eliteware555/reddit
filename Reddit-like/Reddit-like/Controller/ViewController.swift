//
//  ViewController.swift
//  Reddit-like
//
//  Created by Victory on 04/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit
import JLToast

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, DraggableViewBackgroundDelegate {
    
    // Hamburger
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    // share button
    @IBOutlet weak var btnShare: UIBarButtonItem!

    // tab button array
    var mainTabs = [UIButton]()
    @IBOutlet weak var btnHome: UIButton!
    @IBOutlet weak var btnHistory: UIButton!
    @IBOutlet weak var btnTrending: UIButton!
    @IBOutlet weak var btnFavorite: UIButton!
    
    // tab image array
    
    var arrMainTabImages = [UIImageView]()
    @IBOutlet weak var imvHome: UIImageView!
    @IBOutlet weak var imvHistory: UIImageView!
    @IBOutlet weak var imvTrending: UIImageView!
    @IBOutlet weak var imvFavorite: UIImageView!
    
    
    var searchOffset = 0
    var historyOffset = 0
    var trendingOffset = 0
    var favoriteOffset = 0

//    CarbonSwipeRefresh *refreshControl;
    
    // linked list
    // home = 0, history = 1, trending = 2, favorite = 3, search = 4
    var selectedTabIdx: Int = 0
    
    var searchList: [LinkModel] = []
    var historyList: [LinkModel] = []
    var trendingList: [LinkModel] = []
    var favoriteList: [LinkModel] = []
    
    var lblTitle: UILabel!
    var whiteBoldAttributes: [String: AnyObject] = [:]
    var whiteAttributes: [String: AnyObject] = [:]
    
    var isFirstLoading: [Bool] = []
    
    // linklist tableview
    @IBOutlet weak var linkedList: UITableView!
    @IBOutlet weak var linkListSuperView: UIView!
    
    // it will be home, history, trending, favorite, search
    var linkType = ""
    
    // it will be subcategory or search string
    var query = ""
    
    // refresh control
    var refreshControl: CarbonSwipeRefresh!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var draggableBackground: DraggableViewBackground?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // add main tab buttons in mainTabs array
        mainTabs.append(btnHome)
        mainTabs.append(btnHistory)
        mainTabs.append(btnTrending)
        mainTabs.append(btnFavorite)
        
        arrMainTabImages.append(imvHome)
        arrMainTabImages.append(imvHistory)
        arrMainTabImages.append(imvTrending)
        arrMainTabImages.append(imvFavorite)
        
        // white bold & white attributes for title label
        whiteBoldAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 17.0)!]
        whiteAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Helvetica", size: 15.0)!]
        
        initVar()
        
        // initialize view
        initView()
    }
    
    // initialze variables
    func initVar() {
        
        isFirstLoading.append(false)    // home swipe card view
        isFirstLoading.append(false)    // history list
        isFirstLoading.append(false)    // trending list
        isFirstLoading.append(false)    // favorite list
        isFirstLoading.append(false)    // search list
    }
    
    // initialize view
    func initView() {
     
        // navigation title label with two line & custom font
        lblTitle = UILabel(frame: CGRectMake(0, 0, 100, 44))
        lblTitle.backgroundColor = UIColor.clearColor()
        lblTitle.numberOfLines = 2
        lblTitle.textAlignment = .Center
        
        self.navigationItem.titleView = lblTitle
        
        // initialize side menu
        if self.revealViewController() != nil {
            
            if (self.revealViewController().rightViewController != nil) {
                self.revealViewController().rightViewController = nil
            }
            
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(self.revealViewController().revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        // auto cell size table view & remove tableview seperator of empty cell
        linkedList.estimatedRowHeight = 114
        linkedList.rowHeight = UITableViewAutomaticDimension

        linkedList.tableFooterView = UIView(frame: CGRectZero)
        
        // add refresh control
        refreshControl = CarbonSwipeRefresh(scrollView: linkedList)
        refreshControl.setMarginTop(0)
        refreshControl.colors = [UIColor.blackColor(), UIColor.blueColor(), UIColor.greenColor()]
        [self.linkListSuperView .addSubview(refreshControl)]
        
        refreshControl.addTarget(self, action: #selector(refresh(_:)), forControlEvents: .ValueChanged)
        
        // add reload control ( pull up refresh)
        linkedList.addFooterWithTarget(self, action: #selector(loadMore), withIndicatorColor: UIColor(red: 210/255.0, green: 210/255.0, blue: 210/255.0, alpha: 1.0))
        
        print("swipe view added!")

        draggableBackground = DraggableViewBackground(frame: CGRectMake(0, 0, self.view.frame.width, self.linkListSuperView.frame.height))
        draggableBackground?.delegate = self

        draggableBackground?.query = self.query

        self.linkListSuperView.addSubview(draggableBackground!)

//        self.isFirstLoading[0] = true
        
        // select current tap ( 0 .. 4)
        selectKindTap(selectedTabIdx)
        
        (self.revealViewController().rearViewController as! MenuViewController).linkListVC = self
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refresh(sender: AnyObject) {
        
        let user_id = UserDefault.getIntValue(R.pref_user_id)
        let token = UserDefault.getString(R.pref_user_token)
        
        let offset = 0
        
        switch selectedTabIdx {
        case 0:
            return
            
        case 1:
            historyOffset  = 0
            break
            
        case 2:
            trendingOffset = 0
            break
            
        case 3:
            favoriteOffset = 0
            break
            
        default:
            searchOffset = 0
            break
        }
        
        if (selectedTabIdx != 4) {
            
            LinkAPI.getLinks(user_id, token: token, type: linkType, offset: offset, length: R.OFFSET, sortCategory: "all"
                , sucess: { (message) in
                    
                    switch self.selectedTabIdx {
                        
                    case 0:
                        break
                        
                    case 1:
                        self.historyList.removeAll()
                        
                        break
                        
                    case 2:
                        
                        self.trendingList.removeAll()
                        break
                        
                    default:
                        
                       self.favoriteList.removeAll()
                        break
                    }
                    
                    for index in 0 ..< message.count {
                        
                        switch self.selectedTabIdx {
                            
                        case 0:
                            break
                            
                        case 1:
                            self.historyList.append(message[index])
                            break
                            
                        case 2:
                            self.trendingList.append(message[index])
                            break
                            
                        default:
                            self.favoriteList.append(message[index])
                            break
                        }
                    }
                    
                    self.isFirstLoading[self.selectedTabIdx] = true
                    
                    self.endRefreshing()
                    
                }
                , failure:  { (message) in
                    
                    self.endRefreshing()
                    
                    self.linkedList.reloadData()
                    
                    JLToast.makeText(message, duration: 2.0).show()
            })
            
        } else {
            
            LinkAPI.getLinksWithQuery(user_id, token: token, type: "search", offset: searchOffset, length: R.OFFSET, sortCategory: "all", query: self.query
                , sucess: { (message) in
                    
                    self.searchList.removeAll()
                    
                    for index in 0 ..< message.count {
                        
                        self.searchList.append(message[index])
                    }
                    
                    self.isFirstLoading[self.selectedTabIdx] = true
                    
                    self.endRefreshing()
                }
                , failure:  { (message) in
                    
                    JLToast.makeText(message, duration: 1.0).show()
                    
                    self.endRefreshing()
            })
        }
    }
    
    func endRefreshing() {
        
        refreshControl.endRefreshing()
        linkedList.reloadData()
    }
   
    // load new data ( to smulate getting data from server)
    // define constantly as a simple
    // you can customize it as you want according to back end api an as your requirement
    func loadMore() {
        
        getLinks(linkType)
    }
    
    // update tab selection state
    func updateTabSelection(selectedIdx: Int) {
        
        for button in mainTabs {
            
            button.backgroundColor = UIColor(red: 210/255.0, green: 210/255.0, blue: 210/255.0, alpha: 1.0)
        }
        
        for imageview in arrMainTabImages {
            
            imageview.image = imageview.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            imageview.tintColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
        }
        
        // if linkType = "search", no tab selection.
        if (selectedIdx == 4) {
            
            return
        }
        
        let button = mainTabs[selectedIdx]
        button.backgroundColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
        
        let imageview = arrMainTabImages[selectedIdx]
        imageview.image = imageview.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        imageview.tintColor = UIColor.whiteColor()
    }
    
    // update navitaion sub title according to selected tab
    func updateNavTitle(selectedIdx: Int) {
        
        var indexPathRow = selectedTabIdx
        
        var strSubTitle = ""
        
        switch selectedIdx {
            
        case 0:     // home
            
            strSubTitle = "home"
            linkType = "recommended"
            
            switch self.query {
                
            case "worldnews":
                
                indexPathRow = 5
                strSubTitle = "World News"
                
                break
                
            case "politics":
                
                indexPathRow = 6
                strSubTitle = "Politics"
                
                break
                
            case "sports":
                
                indexPathRow = 7
                strSubTitle = "Sports"
                
                break
                
            case "entertainment":
                
                indexPathRow = 8
                strSubTitle = "Entertainment"
                
                break
                
            case "technology":
                
                indexPathRow = 9
                strSubTitle = "Tochnology"
                
                break
            case "business":
                
                indexPathRow = 10
                strSubTitle = "Business"
                
                break
                
            case "science":
                
                indexPathRow = 11
                strSubTitle = "Science"
                
                break
                
            case "environment":
                
                indexPathRow = 12
                strSubTitle = "Environment"
                
                break
                
            case "lifstyle":
                
                indexPathRow = 13
                strSubTitle = "Lifestyle"
                
                break
            default: ()
                
            }
            
            break
            
        case 1:         // history
            
            strSubTitle = "history"
            linkType = "history"
            
            break
            
        case 2:         // trending
            
            strSubTitle = "trending"
            linkType = "trending"
            
            break
            
        case 3:         // favorite
            
            strSubTitle = "favorites"
            linkType = "favorites"
            
            break
            
        default:        // search
            
            strSubTitle = "search"
            
            break
        }
        
        let strTitle = "pics\("\n")" + strSubTitle
        
        let attributedTitle = NSMutableAttributedString(string: strTitle)
        attributedTitle.addAttributes(whiteBoldAttributes, range: NSMakeRange(0, 4))
        attributedTitle.addAttributes(whiteAttributes, range: NSMakeRange(5, strSubTitle.characters.count))
        lblTitle.attributedText = attributedTitle
        
        (self.revealViewController().rearViewController as! MenuViewController).selectedIndexPath
            = NSIndexPath(forRow: indexPathRow, inSection: 0)
    }
    
    func selectKindTap(selectedIdx: Int) {
        
        // change selected index
        selectedTabIdx = selectedIdx
        
        switch selectedIdx {
            
        case 0:
            
            // update navigation title
            updateNavTitle(0)
            
            // update tab selection
            updateTabSelection(0)
            
            draggableBackground?.hidden = false
            
            if (!isFirstLoading[0]) {
                
                draggableBackground?.query = self.query
                draggableBackground?.fetchAndLoadCards()
                
                isFirstLoading[0] = true
            }
            
//            if (draggableBackground == nil) {
//                
//                print("swipe view added!")
//                
//                draggableBackground = DraggableViewBackground(frame: CGRectMake(0, 0, self.view.frame.width, self.linkListSuperView.frame.height))
//                draggableBackground?.delegate = self
//                
//                draggableBackground?.query = self.query
//                
//                self.linkListSuperView.addSubview(draggableBackground!)
//                
//                self.isFirstLoading[0] = true
//                
//            } else {
//                
//                draggableBackground?.hidden = false
//            }
//            
//            if (!isFirstLoading[0]) {
//                
//                draggableBackground?.query = self.query
//                
//                draggableBackground?.fetchAndLoadCards()
//                
//                isFirstLoading[0] = true
//            }
            
            break
            
        default:
            
            if (draggableBackground != nil) {
                
                draggableBackground!.hidden = true
            }
            
            // update navigation title
            updateNavTitle(selectedIdx)
            
            // update tab selection
            updateTabSelection(selectedIdx)
           
            if (!isFirstLoading[selectedIdx]) {
                
                getLinks(linkType)
                
            } else {
                
                linkedList.reloadData()
            }
            
            break
        }
    }
    
    // MARK : main tab
    @IBAction func maintabTapped(sender: UIButton) {
        
        switch sender {
            
        case btnHome:
            
            if (selectedTabIdx == 0) {
                return
            }
            
            selectKindTap(0)
            
            break
            
        case btnHistory:
            
            if (selectedTabIdx == 1) {
                return
            }
            
            selectKindTap(1)
            
            break
            
        case btnTrending:
            
            if (selectedTabIdx == 2) {
                return
            }
            
            selectKindTap(2)
            
            break

        default:
            
            if (selectedTabIdx == 3) {
                return
            }
            
            selectKindTap(3)
            
            break
        }
    }
    
    func getLinks(type: String!) {
        
        appDelegate.showLoadingViewWithTitle(nil, sender: self)
        
        let user_id = UserDefault.getIntValue(R.pref_user_id)
        let token = UserDefault.getString(R.pref_user_token)
        
        var offset = 0
        
        switch selectedTabIdx {
            
        case 0:     // no need to get links here
            break
            
        case 1:     // history list
            offset = historyOffset
            break
            
        case 2:     // trending list
            offset = trendingOffset
            break
            
        case 3:     // favorite list
            offset = favoriteOffset
            break
            
        default:    // search list
            offset = searchOffset
            break
        }
        
        if (selectedTabIdx != 4) {
        
            LinkAPI.getLinks(user_id, token: token, type: linkType, offset: offset, length: R.OFFSET, sortCategory: "all"
                , sucess: { (message) in
                    
                    self.appDelegate.hideLoadingView()
                    
                    for index in 0 ..< message.count {
                        
                        switch self.selectedTabIdx {
                            
                        case 0:
                            break
                            
                        case 1:
                            self.historyList.append(message[index])
                            break
                            
                        case 2:
                            self.trendingList.append(message[index])
                            break
                            
                        default:
                            self.favoriteList.append(message[index])
                            break
                        }
                    }
                    
                    switch self.selectedTabIdx {
                        
                    case 0:
                        break
                        
                    case 1:
                        self.historyOffset += 1
                        
                        break
                        
                    case 2:
                        
                        self.trendingOffset += 1
                        break
                        
                    default:
                        
                        self.favoriteOffset += 1
                        break
                    }
                    
                    self.linkedList.footerEndRefreshing()
                    
                    self.linkedList.reloadData()
                    
                    self.isFirstLoading[self.selectedTabIdx] = true
                    
                }
                , failure:  { (message) in
                    
                    self.appDelegate.hideLoadingView()
                    
                    self.linkedList.footerEndRefreshing()
                    
                    // it will show empty list
                    self.linkedList.reloadData()
                    
                    JLToast.makeText(message, duration: 1.0).show()
            })
            
        } else {

            LinkAPI.getLinksWithQuery(user_id, token: token, type: linkType, offset: offset, length: R.OFFSET, sortCategory: "all", query: self.query
                , sucess: { (message) in
                    
                    self.appDelegate.hideLoadingView()
                    
                    for index in 0 ..< message.count {
                        
                        self.searchList.append(message[index])
                    }
                    
                    self.searchOffset += 1
                    
                    self.linkedList.footerEndRefreshing()
                    
                    self.linkedList.reloadData()
                    
                    self.isFirstLoading[self.selectedTabIdx] = true
                    
                }
                , failure:  { (message) in
                    
                    self.appDelegate.hideLoadingView()
                    
                    self.linkedList.footerEndRefreshing()
                    
                    // it will show empty list
                    self.linkedList.reloadData()
                    
                    JLToast.makeText(message, duration: 1.0).show()
            })
        }
    }
    
    // MARK: - UITableView DataSource & Delegate
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch selectedTabIdx {
            
        case 1:     // history list
            
            return historyList.count
            
        case 2:     // treding list
            
            return trendingList.count
            
        case 3:     // favorites list
            
            return favoriteList.count
            
        default:    // search list
            
            return searchList.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("LinkCell") as! LinkCell
        
        switch selectedTabIdx {
            
        case 1:         // history list
            
            cell.setMainModel(historyList[indexPath.row])
            
            cell.btnCircleCheck.tag = 200 + 16*indexPath.row + 4
            cell.btnCircleX.tag = 200 + 16*indexPath.row + 5
            
            cell.btnFavorite.tag = 200 + 16*indexPath.row + 6
            
            cell.btnViewComments.tag = 200 + 16*indexPath.row + 7
            
            break
            
        case 2:         // treding list
        
            cell.setMainModel(trendingList[indexPath.row])

            cell.btnCircleCheck.tag = 200 + 8*indexPath.row + 8
            cell.btnCircleX.tag = 200 + 8*indexPath.row + 9
            
            cell.btnFavorite.tag = 200 + 12*indexPath.row + 10
            
            cell.btnViewComments.tag = 200 + 16*indexPath.row + 11
            
            break
            
        case 3:         // favorites list
            
            cell.setMainModel(favoriteList[indexPath.row])
            
            cell.btnCircleCheck.tag = 200 + 8*indexPath.row + 12
            cell.btnCircleX.tag = 200 + 8*indexPath.row + 13
            
            cell.btnFavorite.tag = 200 + 12*indexPath.row + 14
            
            cell.btnViewComments.tag = 200 + 16*indexPath.row + 15
            
            break
            
        default:        // search list
            
            cell.setMainModel(searchList[indexPath.row])
            
            cell.btnCircleCheck.tag = 200 + 16*indexPath.row
            cell.btnCircleX.tag = 200 + 16*indexPath.row + 1
            
            cell.btnFavorite.tag = 200 + 16*indexPath.row + 2
            
            cell.btnViewComments.tag = 200 + 16*indexPath.row + 3
            
            break
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let user_id = UserDefault.getIntValue(R.pref_user_id)
        let token = UserDefault.getString(R.pref_user_token)
        
        var linkid = 0
        
        switch selectedTabIdx {
            
        case 1:     // history list
            
            linkid = historyList[indexPath.row].linkid
            break
            
        case 2:     // treding list
            
            linkid = trendingList[indexPath.row].linkid
            break
            
        case 3:    // favorites list
            
            linkid = favoriteList[indexPath.row].linkid
            break
            
        default:    // search list
            
            linkid = searchList[indexPath.row].linkid
            break
        }
        
        appDelegate.showLoadingViewWithTitle(nil, sender: self)        
        
        LinkAPI.setLinkAsViewed(user_id, token: token, linkid: linkid
            
            , success: { (statusCode, message) in
                
                self.appDelegate.hideLoadingView()
                
                self.performSegueWithIdentifier("Segue2WebView", sender: self)
                
                JLToast.makeText(message, duration: 1.0).show()
            
            }
            , failure:  { (statusCode, message) in
                
                self.appDelegate.hideLoadingView()
                
                JLToast.makeText(message, duration: 2.0).show()
        })
    }
    
    // update user link - likeValue = 1
    @IBAction func circleCheckTapped(sender: UIButton!) {
        
        var linkid = 0
        
        var linkCell: LinkCell!
        
        switch selectedTabIdx {
            
        case 1:     // history list
            
            linkid = historyList[(sender.tag - 204)/16].linkid
            linkCell = linkedList .cellForRowAtIndexPath(NSIndexPath(forRow: (sender.tag - 203)/16, inSection: 0)) as! LinkCell
            break
            
        case 2:     // treding list
            
            linkid = trendingList[(sender.tag - 208)/16].linkid
            linkCell = linkedList .cellForRowAtIndexPath(NSIndexPath(forRow: (sender.tag - 206)/16, inSection: 0)) as! LinkCell
            break
            
        case 3:    // favorites list
            
            linkid = favoriteList[(sender.tag - 212)/16].linkid
            linkCell = linkedList .cellForRowAtIndexPath(NSIndexPath(forRow: (sender.tag - 209)/16, inSection: 0)) as! LinkCell
            break
            
        default:    // search list
            
            linkid = searchList[(sender.tag - 200)/16].linkid
            linkCell = linkedList .cellForRowAtIndexPath(NSIndexPath(forRow: (sender.tag - 200)/16, inSection: 0)) as! LinkCell
            
            break
        }
        
        
        linkCell.imvCircleCheck.image = linkCell.imvCircleCheck.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        linkCell.imvCircleCheck.tintColor = UIColor.whiteColor()
        linkCell.imvCircleCheck.backgroundColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
        
        
        linkCell.imvCircleX.image = linkCell.imvCircleX.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        linkCell.imvCircleX.tintColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
        linkCell.imvCircleX.backgroundColor = UIColor.whiteColor()
        
        updateUserLink(1, linkid: linkid)
    }
    
    // update user link - likeValue = 0
    @IBAction func circlexTapped(sender: UIButton!) {
        
        var linkid = 0
        
        var linkCell: LinkCell!
        
        switch selectedTabIdx {
            
        case 1:     // history list
            
            linkid = historyList[(sender.tag - 205)/12].linkid
            linkCell = linkedList .cellForRowAtIndexPath(NSIndexPath(forRow: (sender.tag - 204)/12, inSection: 0)) as! LinkCell
            break
            
        case 2:     // treding list
            
            linkid = trendingList[(sender.tag - 209)/12].linkid
            linkCell = linkedList .cellForRowAtIndexPath(NSIndexPath(forRow: (sender.tag - 207)/12, inSection: 0)) as! LinkCell
            break
            
        case 3:    // favorites list
            
            linkid = favoriteList[(sender.tag - 213)/12].linkid
            linkCell = linkedList .cellForRowAtIndexPath(NSIndexPath(forRow: (sender.tag - 210)/12, inSection: 0)) as! LinkCell
            break
            
        default:    // search list
            
            linkid = searchList[(sender.tag - 201)/12].linkid
            linkCell = linkedList .cellForRowAtIndexPath(NSIndexPath(forRow: (sender.tag - 201)/12, inSection: 0)) as! LinkCell
            
            break
        }
        
        linkCell.imvCircleCheck.image = linkCell.imvCircleCheck.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        linkCell.imvCircleCheck.tintColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
        linkCell.imvCircleCheck.backgroundColor = UIColor.whiteColor()
        
        
        linkCell.imvCircleX.image = linkCell.imvCircleX.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        linkCell.imvCircleX.tintColor = UIColor.whiteColor()
        linkCell.imvCircleX.backgroundColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
        
        updateUserLink(-1, linkid: linkid)
    }
    
    // update user link - likeValue - 1, 0
    func updateUserLink(likeValue: Int!, linkid: Int!) {
        
        appDelegate.showLoadingViewWithTitle(nil, sender: self)
        
        let user_id = UserDefault.getIntValue(R.pref_user_id)
        let token = UserDefault.getString(R.pref_user_token)
        
        LinkAPI.updateUserLink(user_id, token: token, linkid: linkid, likevalue: likeValue
            , success: { (statusCode, message) in
                
                JLToast.makeText(message, duration: 2.0).show()
                
                self.appDelegate.hideLoadingView()
            }
            , failure:  { (statusCode, message) in
                
                JLToast.makeText(message, duration: 2.0).show()
                
                self.appDelegate.hideLoadingView()
        })
    }
    
    @IBAction func viewCommentsTapped(sender: UIButton!) {
        
        var linkModel: LinkModel!
        
        switch selectedTabIdx {
            
        case 1:     // history list
            
            linkModel = historyList[(sender.tag - 207)/12]
            
            break
            
        case 2:     // treding list
            
            linkModel = trendingList[(sender.tag - 211)/12]

            
            break
            
        case 3:    // favorites list
            
            linkModel = favoriteList[(sender.tag - 215)/12]
            
            break
            
        default:    // search list
            
            linkModel = searchList[(sender.tag - 203)/12]
            
            break
        }
        
        // go to comment view
        
        let commentVC = self.storyboard?.instantiateViewControllerWithIdentifier("CommentViewController") as! CommentViewController
        commentVC.selectedItem = linkModel
        self.navigationController?.pushViewController(commentVC, animated: true)
    }
    
    @IBAction func favoriteTaped(sender: UIButton!) {
        
        var linkModel: LinkModel!
        
        var isFavorite: Bool!
        
        switch selectedTabIdx {
            
        case 1:     // history list
            
            linkModel = historyList[(sender.tag - 206)/12]
            isFavorite = !historyList[(sender.tag - 205)/12].isFavorite

            break
            
        case 2:     // treding list
            
            linkModel = trendingList[(sender.tag - 210)/12]
            isFavorite = !trendingList[(sender.tag - 208)/12].isFavorite

            break
            
        case 3:    // favorites list
            
            linkModel = favoriteList[(sender.tag - 214)/12]
            isFavorite = !favoriteList[(sender.tag - 211)/12].isFavorite

            break
            
        default:    // search list
            
            linkModel = searchList[(sender.tag - 202)/12]
            isFavorite = !searchList[(sender.tag - 202)/12].isFavorite

            
            break
        }
        
        setUserLinkFavorite(isFavorite, linkModel: linkModel)
    }
    
    func setUserLinkFavorite(isFavorite: Bool!, linkModel: LinkModel!) {
        
        appDelegate.showLoadingViewWithTitle(nil, sender: self)
        
        let user_id = UserDefault.getIntValue(R.pref_user_id)
        let token = UserDefault.getString(R.pref_user_token)
        
        let linkid = linkModel.linkid
        
        LinkAPI.setUserLinkFavorite(user_id, token: token, linkid: linkid, isFavorite: isFavorite
            , success: { (statusCode, message) in
                
                JLToast.makeText(message, duration: 2.0).show()
  
                self.appDelegate.hideLoadingView()
                
                linkModel.isFavorite = isFavorite
                
                self.linkedList.reloadData()
                
            }
            , failure:  { (statusCode, message) in
                
                JLToast.makeText(message, duration: 2.0).show()
                
                self.appDelegate.hideLoadingView()
        })
    }
    
    // MARK: share tapped
    @IBAction func shareTapped(sender: AnyObject) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let favorite = UIAlertAction(title: R.share_favorite, style: .Default) { (alert: UIAlertAction!) -> Void in
            
            // something here what you want
        }
        let copyurl = UIAlertAction(title: R.share_copy_url, style: .Default) { (alert: UIAlertAction!) -> Void in
            
            // something here what you want
        }
        
        let shareopen = UIAlertAction(title: R.share_open, style: .Default) { (alert: UIAlertAction!) -> Void in
            
            // something here what you want
        }
        
        let cancel = UIAlertAction(title: R.share_cencel, style: .Cancel) { (alert: UIAlertAction!) -> Void in
            
            // something here what you want
        }
        
        //        picker?.delegate = self
        
        alert.addAction(favorite)
        alert.addAction(copyurl)
        alert.addAction(shareopen)
        alert.addAction(cancel)
        presentViewController(alert, animated: true, completion: nil)
    }
    
    func gotoWebView(model: LinkModel!) {
        
        let webVC = self.storyboard?.instantiateViewControllerWithIdentifier("WebViewController") as! WebViewController
        webVC.loadItem = model
        self.navigationController?.pushViewController(webVC, animated: true)
    }
    
    func gotoCommentView(link: LinkModel!) {
        
        let commentVC = self.storyboard?.instantiateViewControllerWithIdentifier("CommentViewController") as! CommentViewController
        commentVC.selectedItem = link
        self.navigationController?.pushViewController(commentVC, animated: true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if (segue.identifier == "Segue2WebView") {
            
            let destVC = segue.destinationViewController as! WebViewController
            
            let selectedIndexPath = linkedList.indexPathForSelectedRow!
            
            switch selectedTabIdx {
                
            case 1:     // history list
                
                destVC.loadItem = historyList[selectedIndexPath.row]
                break
                
            case 2:     // treding list
                
                destVC.loadItem = trendingList[selectedIndexPath.row]
                break
                
            case 3:    // favorites list
                
                destVC.loadItem = favoriteList[selectedIndexPath.row]
                break
                
                
            default:    // search list
                
                destVC.loadItem = searchList[selectedIndexPath.row]
                
                break
            }
        }
    }
    
}



