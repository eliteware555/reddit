//
//  LoginViewController.swift
//  Reddit-like
//
//  Created by Victory on 06/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit
import JLToast

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    var tField: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var imvUserImage: UIImageView!
    
    @IBOutlet weak var btnForgotPassword: UIButton!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // navigationbar tint color
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        // remove back button title
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        txtEmail.delegate = self
        txtPassword.delegate = self
        
        initView()
    }
    
    func initView() {
        
        // add left view in textfield
        let leftEmailImageView = UIImageView(frame: CGRectMake(0, 0, 40, 36))
        leftEmailImageView.image = UIImage(named: "ic_mail")
        leftEmailImageView.contentMode = .Center
        
        // only left side corner radius
        let rectShape = CAShapeLayer()
        rectShape.bounds = leftEmailImageView.frame
        rectShape.position = leftEmailImageView.center
        rectShape.path = UIBezierPath(roundedRect: leftEmailImageView.bounds, byRoundingCorners: [.BottomLeft, .TopLeft], cornerRadii: CGSize(width: 5, height: 5)).CGPath
        
        leftEmailImageView.layer.backgroundColor = UIColor(red: 216/255.0, green: 67/255.0, blue: 21/255.0, alpha: 1.0).CGColor
        leftEmailImageView.layer.mask = rectShape
        
        let leftEmailView = UIView(frame: CGRectMake(0, 0, 46, 36))
        leftEmailView.addSubview(leftEmailImageView)
        
        txtEmail.leftView = leftEmailView
        txtEmail.leftViewMode = UITextFieldViewMode.Always
        
        let leftPwdImageView = UIImageView(frame: CGRectMake(0, 0, 40, 36))
        leftPwdImageView.image = UIImage(named: "ic_lock")
        leftPwdImageView.contentMode = .Center
        
        leftPwdImageView.layer.backgroundColor = UIColor(red: 216/255.0, green: 67/255.0, blue: 21/255.0, alpha: 1.0).CGColor
        leftPwdImageView.layer.mask = rectShape
        
        let leftPwdView = UIView(frame: CGRectMake(0, 0, 46, 36))
        leftPwdView.addSubview(leftPwdImageView)
        
        txtPassword.leftView = leftPwdView
        txtPassword.leftViewMode = UITextFieldViewMode.Always
        
        // forgot password button with underline text
        let attrs = [
            NSFontAttributeName : UIFont.systemFontOfSize(16.0),
            NSForegroundColorAttributeName : UIColor.whiteColor(),
            NSUnderlineStyleAttributeName : 1]
        
        let attributedString = NSMutableAttributedString(string:"Forgot Password?", attributes:attrs)
        btnForgotPassword.setAttributedTitle(attributedString, forState: .Normal)
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        // auto login process
        if (UserDefault.getBoolValue(R.pref_auto_login)!) {
            
            txtEmail.text = UserDefault.getString(R.pref_user_email)!
            txtPassword.text = UserDefault.getString(R.pref_user_password)!
            
            if (UserDefault.getString(R.pref_user_image) != nil) {
                
                imvUserImage.setImageWithUrl(NSURL(string: UserDefault.getString(R.pref_user_image)!)!, placeHolderImage: UIImage(named: "user_icon.png"))
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        
        if (UserDefault.getBoolValue(R.pref_auto_login)!) {
        
            doLogin()
        }
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if (textField == txtEmail) {
            
            txtPassword.becomeFirstResponder()
        }
        
        textField.resignFirstResponder()
        
        return  true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func configurationTextField(textField: UITextField!)
    {
        textField.placeholder = "Enter your email address"
        textField.keyboardType = UIKeyboardType.EmailAddress
        tField = textField
    }
    
    // forgot password
    @IBAction func forgotPasswordTapped(sender: UIButton) {
        
        let alert = UIAlertController(title: "Reset Password", message: "Enter your email address to receive pasword reset instructions", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        alert.addAction(UIAlertAction(title: R.alert_cancel, style: UIAlertActionStyle.Cancel, handler:nil))
        alert.addAction(UIAlertAction(title: R.alert_ok, style: UIAlertActionStyle.Default, handler:{ (UIAlertAction)in
            
            if (self.tField.text == "") {
                
                JLToast.makeText(R.input_your_email, duration: 2.0).show()
                return
            }
            
            let strEmail = self.tField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            
            self.appDelegate.showLoadingViewWithTitle(nil, sender: self)
            
            LinkAPI.resetPassword(strEmail
                , success: { (statusCode, message) in
                    
                    self.appDelegate.hideLoadingView()
                    
                    JLToast.makeText(message, duration: 2.0).show()
                    
                }
                , failure: { (statusCode, message) in
                    
                    self.appDelegate.hideLoadingView()
                    
                    JLToast.makeText(message, duration: 2.0).show()
            })
            
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    //  sign in button action
    @IBAction func singinTapped(sender: UIButton) {
        
        self.view.endEditing(true)        
        
        if (checkValid() == true) {
        
            verifyCredentials()
        }
    }
    
    func checkValid() -> Bool! {
        
        if (txtEmail.text?.isEmpty)! {
            
            appDelegate.showAlertDialog(nil, message: R.input_your_email, positive: R.alert_ok, negative: nil, sender: self)
            
            return false
            
        } else if (!CommonUtils.isValidEmail(txtEmail.text)) {
            
            appDelegate.showAlertDialog(nil, message: R.input_correct_email, positive: R.alert_ok, negative: nil, sender: self)
            
            return false
            
        } else if (txtPassword.text?.isEmpty)! {
        
            appDelegate.showAlertDialog(nil, message: R.input_password, positive: R.alert_ok, negative: nil, sender: self)
            
            return false
            
        } else if (txtPassword.text?.characters.count < 8) {
            
            appDelegate.showAlertDialog(nil, message: R.password_length, positive: R.alert_ok, negative: nil, sender: self)
            
            return false
        }
        
        return true
    }
    
    func verifyCredentials() {
        
        appDelegate.showLoadingViewWithTitle(nil, sender: self)
        
        let email = txtEmail.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let password = txtPassword.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        LinkAPI.verifyCredentials(email, password: password
            , success: { (statusCode, message, user_id, user_name, token, tokenExpiration) in
                
                JLToast.makeText(message, duration: 1.0).show()
                
                // save user credentials
                UserDefault.setString(user_name, key: R.pref_user_name)
                UserDefault.setString(email, key: R.pref_user_email)
                UserDefault.setString(password, key: R.pref_user_password)
                
                UserDefault.setIntValue(user_id, key: R.pref_user_id)
                UserDefault.setString(token, key: R.pref_user_token)
                UserDefault.setString(tokenExpiration, key: R.pref_expiration)
                
                // verify token
                self.verifyToken(user_id, token: token)
            
            }) { (statusCode, message) in
                
                self.appDelegate.hideLoadingView()
                
                JLToast.makeText(message, duration: 2.0).show()
        }
    }
    
    func doLogin() {
        
        // check if token is expired
        
        appDelegate.showLoadingViewWithTitle(nil, sender: self)
        
        if (CommonUtils.tokenExpired()) {
         
            getToken()
            
        } else {
            
            let user_id = UserDefault.getIntValue(R.pref_user_id)
            let token = UserDefault.getString(R.pref_user_token)
            
            // verify token
            self.verifyToken(user_id, token: token)
        }
    }
    
    func getToken() {

        let username = UserDefault.getString(R.pref_user_name)!
        let password = UserDefault.getString(R.pref_user_password)!
        let email = UserDefault.getString(R.pref_user_email)!
        
        LinkAPI.getToken(username, password: password, email: email
            , success: { (statusCode, message, user_id, token, tokenExpiration) in
                
                JLToast.makeText(message, duration: 1.0).show()
                
                // save user credentials
                UserDefault.setIntValue(user_id, key: R.pref_user_id)
                UserDefault.setString(token, key: R.pref_user_token)
                UserDefault.setString(tokenExpiration, key: R.pref_expiration)
                
                // verify token
                self.verifyToken(user_id, token: token)
            
            }) { (statusCode, message) in
                
                self.appDelegate.hideLoadingView()
                
                JLToast.makeText(message, duration: 2.0).show()
        }
    }
    
    func verifyToken(user_id: Int!, token: String!) {
        
        LinkAPI.verifyToken(user_id, token: token
            , success: { (statusCode, message) in
                
                JLToast.makeText(message, duration: 2.0).show()
                
                if (UserDefault.getBoolValue(R.pref_auto_login)!) {
                
                    self.appDelegate.hideLoadingView()
                    
                    self.gotoMainView()
                    
                } else {
                 
                    self.getUserImage(user_id, token: token)
                }
                
            }, failure: { (statusCode, message) in
                
                self.appDelegate.hideLoadingView()
                
                JLToast.makeText(message, duration: 2.0).show()
        })
    }
    
    func getUserImage(user_id: Int!, token: String!) {
        
        LinkAPI.getUserImage(user_id, token: token
            , success: { (statusCode, message) in
                
                JLToast.makeText(message, duration: 2.0).show()
                
                self.appDelegate.hideLoadingView()
                
                self.gotoMainView()
                
            }, failure:  { (statusCode, message) in
                
                self.appDelegate.hideLoadingView()
                
                self.gotoMainView()
                
                JLToast.makeText(message, duration: 2.0).show()
        })
    }
    
    // redirect to main linked view
    func gotoMainView() {
        
        UserDefault.setBoolValue(true, key: R.pref_auto_login)
        
        let revealVC = self.storyboard?.instantiateViewControllerWithIdentifier("SWReveal") as! SWRevealViewController
        
        UIApplication.sharedApplication().keyWindow?.rootViewController = revealVC
    }
}
