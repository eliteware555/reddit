//
//  MenuViewController.swift
//  Reddit-like
//
//  Created by Victory on 04/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

//973a35

import UIKit
import JLToast

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UISearchBarDelegate {
    
    // propfile image view
    @IBOutlet weak var imvProfile: UIImageView!
    // UIImagePickerController
    var picker: UIImagePickerController? = UIImagePickerController()
    // saved profile image path
    var strPhotoPath = ""
    
    // searchbar
    @IBOutlet weak var searchField:UISearchBar!
    
    // expandable side menu
    var expandedState: Int = 0
    var arrExpandedFull = [MenuModel]()
    var arrContractOne = [MenuModel]()    
    
    // selected menu index
    var selectedIndexPath = NSIndexPath(forRow: 0, inSection: 0)
    
    // user image
    @IBOutlet weak var userImage: UIImageView!
    
    // user name label
    @IBOutlet weak var lblUserName: UILabel!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    var isMenuReload = false
    
    var lastMenuString = ""
    
    
    @IBOutlet weak var menuList: UITableView!
    
    // LinkListView - ViewController
    var linkListVC: ViewController!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        userImage.setImageWithUrl(NSURL(string: UserDefault.getString(R.pref_user_image)!)!, placeHolderImage: UIImage(named: "1.png"))

        // Do any additional setup after loading the view.
        
        arrExpandedFull.append(MenuModel(lImage: UIImage(named: "menu_home"), rImage: UIImage(named: "menu_home")!, item: "HOME", isSubMenu: false))
        arrExpandedFull.append(MenuModel(lImage: UIImage(named: "menu_history"), rImage: UIImage(named: "menu_history")!, item: "HISTORY", isSubMenu: false))
        arrExpandedFull.append(MenuModel(lImage: UIImage(named: "menu_trending"), rImage: UIImage(named: "menu_trending")!, item: "TRENDING", isSubMenu: false))
        arrExpandedFull.append(MenuModel(lImage: UIImage(named: "menu_favorite"), rImage: UIImage(named: "menu_favorite")!, item: "FAVORITES", isSubMenu: false))
        arrExpandedFull.append(MenuModel(lImage: UIImage(named: "menu_category"), rImage: UIImage(named: "menu_category")!, item: "CATEGORIES", isSubMenu: false))
        arrExpandedFull.append(MenuModel(lImage: UIImage(named: "menu_worldnews"), rImage: UIImage(named: "menu_worldnews")!, item: "World News", isSubMenu: true))
        arrExpandedFull.append(MenuModel(lImage: UIImage(named: "menu_politics"), rImage: UIImage(named: "menu_politics")!, item: "Politics", isSubMenu: true))
        arrExpandedFull.append(MenuModel(lImage: UIImage(named: "menu_sports"), rImage: UIImage(named: "menu_sports")!, item: "Sports", isSubMenu: true))
        arrExpandedFull.append(MenuModel(lImage: UIImage(named: "menu_entertainment"), rImage: UIImage(named: "menu_entertainment")!, item: "Entertainment", isSubMenu: true))
        arrExpandedFull.append(MenuModel(lImage: UIImage(named: "menu_tech"), rImage: UIImage(named: "menu_tech")!, item: "Technology", isSubMenu: true))
        arrExpandedFull.append(MenuModel(lImage: UIImage(named: "menu_business"), rImage: UIImage(named: "menu_business")!, item: "Business", isSubMenu: true))
        arrExpandedFull.append(MenuModel(lImage: UIImage(named: "menu_science"), rImage: UIImage(named: "menu_science")!, item: "Science", isSubMenu: true))
        arrExpandedFull.append(MenuModel(lImage: UIImage(named: "menu_environment"), rImage: UIImage(named: "menu_environment")!, item: "Environment", isSubMenu: true))
        arrExpandedFull.append(MenuModel(lImage: UIImage(named: "menu_lifestyle"), rImage: UIImage(named: "menu_lifestyle")!, item: "Lifstyle", isSubMenu: true))
        arrExpandedFull.append(MenuModel(lImage: UIImage(named: "menu_lock"), rImage: UIImage(named: "menu_lock")!, item: "CHANGE PASSWORD", isSubMenu: false))
        arrExpandedFull.append(MenuModel(lImage: UIImage(named: "menu_signout"), rImage: UIImage(named: "menu_signout")!, item: "Sign Out", isSubMenu: false))
        
        arrContractOne.append(MenuModel(lImage: UIImage(named: "menu_home"), rImage: UIImage(named: "menu_home")!, item: "HOME", isSubMenu: false))
        arrContractOne.append(MenuModel(lImage: UIImage(named: "menu_history"), rImage: UIImage(named: "menu_history")!, item: "HISTORY", isSubMenu: false))
        arrContractOne.append(MenuModel(lImage: UIImage(named: "menu_trending"), rImage: UIImage(named: "menu_trending")!, item: "TRENDING", isSubMenu: false))
        arrContractOne.append(MenuModel(lImage: UIImage(named: "menu_favorite"), rImage: UIImage(named: "menu_favorite")!, item: "FAVORITES", isSubMenu: false))
        arrContractOne.append(MenuModel(lImage: UIImage(named: "menu_category"), rImage: UIImage(named: "menu_category")!, item: "CATEGORIES", isSubMenu: false))
        arrContractOne.append(MenuModel(lImage: UIImage(named: "menu_lock"), rImage: UIImage(named: "menu_lock")!, item: "CHANGE PASSWORD", isSubMenu: false))
        arrContractOne.append(MenuModel(lImage: UIImage(named: "menu_signout"), rImage: UIImage(named: "menu_signout")!, item: "Sign Out", isSubMenu: false))
        
        lblUserName.text = UserDefault.getString(R.pref_user_name)!
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        // change selection state
        isMenuReload = false
        
        menuList.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UISearchBar Delegate
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
        
        let destNav = self.storyboard?.instantiateViewControllerWithIdentifier("LinkListNav") as! UINavigationController
        let destVC = destNav.topViewController as! ViewController
        
        destVC.selectedTabIdx = 4
        destVC.linkType = "search"
        destVC.query = (searchBar.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()))!
        
        self.revealViewController().pushFrontViewController(destNav, animated: true)
    }
    
    // MARK: - UITableView DataSource and Delegate
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (expandedState == 0) {
            
            return arrExpandedFull.count
            
        } else {
            
            return arrContractOne.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if (expandedState == 0) {
        
            if (indexPath.row == 4) {
                
                let cell = tableView.dequeueReusableCellWithIdentifier("GroupCell") as! GroupCell
                
                cell.lblMenuItem.text = arrExpandedFull[indexPath.row].item
            
                return cell
                
            } else {
        
                let cell = tableView.dequeueReusableCellWithIdentifier("MenuCell") as! MenuCell
                
                cell.setMenuModel(arrExpandedFull[indexPath.row])
                
                cell.contentView.backgroundColor = UIColor.clearColor()
                
                if (indexPath == selectedIndexPath) {
                    
                    cell.contentView.backgroundColor = UIColor(red: 230/255.0, green: 74/255.0, blue: 25/255.0, alpha: 1.0)
                }
                
                return cell
            }
        
        } else {
            
            // first category is contracted (  numberofRows = 6)
            
            if (indexPath.row == 4) {
                
                let cell = tableView.dequeueReusableCellWithIdentifier("GroupCell") as! GroupCell
                
                cell.lblMenuItem.text = arrContractOne[indexPath.row].item
                
                return cell
                
            } else {
                
                let cell = tableView.dequeueReusableCellWithIdentifier("MenuCell") as! MenuCell
                
                cell.setMenuModel(arrContractOne[indexPath.row])
                
                cell.contentView.backgroundColor = UIColor.clearColor()
                
                if (indexPath.row == 5) {
                    
                    if  (indexPath == selectedIndexPath && !isMenuReload && lastMenuString.containsString("CHANGE")) {
                        
                        cell.contentView.backgroundColor = UIColor(red: 230/255.0, green: 74/255.0, blue: 25/255.0, alpha: 1.0)
                    }
                    
                } else if (indexPath.row < 4) {
                
                    if (indexPath == selectedIndexPath) {
                        
                        cell.contentView.backgroundColor = UIColor(red: 230/255.0, green: 74/255.0, blue: 25/255.0, alpha: 1.0)
                    }
                }
                
                return cell
            }
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if (searchField.isFirstResponder()) {
            searchField.resignFirstResponder()
        }
        
        if (indexPath.row == 4) {
            
            if (expandedState == 1) {
                
                expandedState = 0
                
                UIView.animateWithDuration(0.3, animations: {

                    let cell = tableView .cellForRowAtIndexPath(indexPath) as! GroupCell
                    cell.imvLIcon.transform = CGAffineTransformMakeRotation(0)
                })
                
                if (selectedIndexPath.row == 5 && lastMenuString.containsString("CHANGE")) {
                    
                    selectedIndexPath = NSIndexPath(forRow: 14, inSection: 0)
                }
                
            } else {
                
                expandedState = 1
                
                UIView.animateWithDuration(0.3, animations: {
                    
                    let cell = tableView .cellForRowAtIndexPath(indexPath) as! GroupCell
                    cell.imvLIcon.transform = CGAffineTransformMakeRotation(CGFloat.init(M_PI - 0.00001))
                })
                
                if (selectedIndexPath.row > 4 && selectedIndexPath.row < 14) {
                    
                    isMenuReload = true
                }
                
                if (selectedIndexPath.row == 14) {
                    
                    selectedIndexPath = NSIndexPath(forRow: 5, inSection: 0)
                }
            }
            
            tableView.reloadData()
            
            return
        }
        
        // destination view controller where to go currently ( it can be viewcontroller for showing links list or view controller for changeing password)
        var destVC = self.storyboard?.instantiateViewControllerWithIdentifier("LinkListNav") as! UINavigationController!
        
        //  we have 16 menus here (home... signout)
        if (expandedState == 0) {
            
            lastMenuString = arrExpandedFull[indexPath.row].item
            
            switch indexPath.row {
                
            case 0:
                
                // open home view
                if (selectedIndexPath.row > 13) {
                    
                    (destVC.topViewController as! ViewController).selectedTabIdx = 0
                    (destVC.topViewController as! ViewController).query = ""
                    
                } else {
                    
                    linkListVC.selectedTabIdx = 0
                    linkListVC.query = ""
                    
                    if (selectedIndexPath.row > 4 && selectedIndexPath.row < 14) {
                        
                        linkListVC.isFirstLoading[0] = false
                    }
                }
                
                break
                
            case 1:
                
                // open history view
                if (selectedIndexPath.row > 13) {
                    
                    (destVC.topViewController as! ViewController).selectedTabIdx = 1
                    (destVC.topViewController as! ViewController).query = ""
                    
                } else {
                    
                    linkListVC.selectedTabIdx = 1
                    linkListVC.query = ""
                }
                
                break
                
            case 2:
                
                // open trending view
                if (selectedIndexPath.row > 13) {
                    
                    (destVC.topViewController as! ViewController).selectedTabIdx = 2
                    (destVC.topViewController as! ViewController).query = ""
                    
                } else {
                    
                    linkListVC.selectedTabIdx = 2
                    linkListVC.query = ""
                }
                
                break
                
            case 3:
                
                // open favorite view
                if (selectedIndexPath.row > 13) {
                    
                    (destVC.topViewController as! ViewController).selectedTabIdx = 3
                    (destVC.topViewController as! ViewController).query = ""
                    
                } else {
                    
                    linkListVC.selectedTabIdx = 3
                    linkListVC.query = ""
                }
                
                break
                
            case 5:
                // open home view with query "worldnews"
                if (selectedIndexPath.row > 13) {
                    
                    (destVC.topViewController as! ViewController).selectedTabIdx = 0
                    (destVC.topViewController as! ViewController).query = "worldnews"
                    
                } else {
                    
                    linkListVC.selectedTabIdx = 0
                    linkListVC.query = "worldnews"
                    
                    if (selectedIndexPath != 5) {
                        
                        linkListVC.isFirstLoading[0] = false
                    }
                }
                
                break
                
            case 6:
                // open home view with query "politics"
                if (selectedIndexPath.row > 13) {
                    
                    (destVC.topViewController as! ViewController).selectedTabIdx = 0
                    (destVC.topViewController as! ViewController).query = "politics"
                    
                } else {
                    
                    linkListVC.selectedTabIdx = 0
                    linkListVC.query = "politics"
                    
                    if (selectedIndexPath != 6) {
                        
                        linkListVC.isFirstLoading[0] = false
                    }
                }
                
                break
                
            case 7:
                // open home view with query "sports"
                if (selectedIndexPath.row > 13) {
                    
                    (destVC.topViewController as! ViewController).selectedTabIdx = 0
                    (destVC.topViewController as! ViewController).query = "sports"
                    
                } else {
                    
                    linkListVC.selectedTabIdx = 0
                    linkListVC.query = "sports"
                    
                    if (selectedIndexPath != 7) {
                        
                        linkListVC.isFirstLoading[0] = false
                    }
                }
                
                break
                
            case 8:
                // open home view with query "entertainment"
                if (selectedIndexPath.row > 13) {
                    
                    (destVC.topViewController as! ViewController).selectedTabIdx = 0
                    (destVC.topViewController as! ViewController).query = "entertainment"
                    
                } else {
                    
                    linkListVC.selectedTabIdx = 0
                    linkListVC.query = "entertainment"
                    
                    if (selectedIndexPath != 8) {
                        
                        linkListVC.isFirstLoading[0] = false
                    }
                }
                
                break
                
            case 9:
                // open home view with query "technology"
                if (selectedIndexPath.row > 13) {
                    
                    (destVC.topViewController as! ViewController).selectedTabIdx = 0
                    (destVC.topViewController as! ViewController).query = "technology"
                    
                } else {
                    
                    linkListVC.selectedTabIdx = 0
                    linkListVC.query = "technology"
                    
                    if (selectedIndexPath != 9) {
                        
                        linkListVC.isFirstLoading[0] = false
                    }
                }
                
                break
                
            case 10:
                // open home view with query "business"
                if (selectedIndexPath.row > 13) {
                    
                    (destVC.topViewController as! ViewController).selectedTabIdx = 0
                    (destVC.topViewController as! ViewController).query = "business"
                    
                } else {
                    
                    linkListVC.selectedTabIdx = 0
                    linkListVC.query = "business"
                    
                    if (selectedIndexPath != 10) {
                        
                        linkListVC.isFirstLoading[0] = false
                    }
                }
                
                break
                
            case 11:
                // open home view with query "science"
                if (selectedIndexPath.row > 13) {
                    
                    (destVC.topViewController as! ViewController).selectedTabIdx = 0
                    (destVC.topViewController as! ViewController).query = "science"
                    
                } else {
                    
                    linkListVC.selectedTabIdx = 0
                    linkListVC.query = "science"
                    
                    if (selectedIndexPath != 11) {
                        
                        linkListVC.isFirstLoading[0] = false
                    }
                }
                
                break
                
            case 12:
                // open home view with query "environment"
                if (selectedIndexPath.row > 13) {
                    
                    (destVC.topViewController as! ViewController).selectedTabIdx = 0
                    (destVC.topViewController as! ViewController).query = "environment"
                    
                } else {
                    
                    linkListVC.selectedTabIdx = 0
                    linkListVC.query = "environment"
                    
                    if (selectedIndexPath != 12) {
                        
                        linkListVC.isFirstLoading[0] = false
                    }
                }
                
                break
                
            case 13:
                // open home view with query "lifstyle"
                if (selectedIndexPath.row > 13) {
                    
                    (destVC.topViewController as! ViewController).selectedTabIdx = 0
                    (destVC.topViewController as! ViewController).query = "lifstyle"
                    
                } else {
                    
                    linkListVC.selectedTabIdx = 0
                    linkListVC.query = "lifstyle"
                    
                    if (selectedIndexPath != 13) {
                        
                        linkListVC.isFirstLoading[0] = false
                    }
                }
                
                break
                
            case 14:
                
                // change password
                destVC = self.storyboard?.instantiateViewControllerWithIdentifier("ChangePwdNav") as! UINavigationController
                
                break
                
            default:
                
                // sign out
                
                // clear user credentials
                UserDefault.setString("", key: R.pref_user_name)
                UserDefault.setString("", key: R.pref_user_email)
                UserDefault.setString("", key: R.pref_user_password)
                UserDefault.setString("", key: R.pref_user_image)
                UserDefault.setString("", key: R.pref_user_token)
                UserDefault.setString("", key: R.pref_expiration)
                
                UserDefault.setBoolValue(false, key: R.pref_auto_login)
                
                // go to login view
                destVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginNav") as! UINavigationController
                
                UIApplication.sharedApplication().keyWindow?.rootViewController = destVC
                
                return
            }
            
        } else { // we have 7 menus here (home, hostory, trending, favorites, categories, change password, signout)
            
            lastMenuString = arrContractOne[indexPath.row].item
            
            switch indexPath.row {
                
            case 0:
                
                // open home view
                if (selectedIndexPath.row > 4) {
                    
                    (destVC.topViewController as! ViewController).selectedTabIdx = 0
                    (destVC.topViewController as! ViewController).query = ""
                    
                } else {
                    
                    linkListVC.selectedTabIdx = 0
                    linkListVC.query = ""
                    
                    if (selectedIndexPath.row > 4 && selectedIndexPath.row < 14) {
                        
                        linkListVC.isFirstLoading[0] = false
                    }
                }
                
                break
                
            case 1:
                
                // open history view
                if (selectedIndexPath.row > 4) {
                    
                    (destVC.topViewController as! ViewController).selectedTabIdx = 1
                    (destVC.topViewController as! ViewController).query = ""
                    
                } else {
                    
                    linkListVC.selectedTabIdx = 1
                    linkListVC.query = ""
                }
                
                break
                
            case 2:
                
                // open trending view
                if (selectedIndexPath.row > 4) {
                    
                    (destVC.topViewController as! ViewController).selectedTabIdx = 2
                    (destVC.topViewController as! ViewController).query = ""
                    
                } else {
                    
                    linkListVC.selectedTabIdx = 2
                    linkListVC.query = ""
                }
                
                break
                
            case 3:
                
                // open favorite view
                if (selectedIndexPath.row > 4) {
                    
                    (destVC.topViewController as! ViewController).selectedTabIdx = 3
                    (destVC.topViewController as! ViewController).query = ""
                    
                } else {
                    
                    linkListVC.selectedTabIdx = 3
                    linkListVC.query = ""
                }
                
                break
                
            case 5:
                
                // change password
                destVC = self.storyboard?.instantiateViewControllerWithIdentifier("ChangePwdNav") as! UINavigationController
                
                break
                
            default:
                // sign out
                
                //            self.revealViewController().pushFrontViewController(destVC, animated: true)
                
                return
            }
        }
        
//        if (expandedState == 1 && indexPath.row == 5 && !lastMenuString.containsString("CAHNGE")) {
//            
//            
//            
//        } else
        if (indexPath == selectedIndexPath) {
            
            var toggle = true
            
            if (indexPath.row == 5 && expandedState == 1 && !lastMenuString.containsString("CAHNGE")) {
                
                toggle = false
            }
            
            if (toggle) {
              
                self.revealViewController().revealToggleAnimated(true)
                return
                
            }
        }
        
        if let oldSelectedCell: UITableViewCell = tableView.cellForRowAtIndexPath(selectedIndexPath) {
            
            oldSelectedCell.contentView.backgroundColor = UIColor.clearColor()
        }
        
        let selectedCell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        selectedCell.contentView.backgroundColor = UIColor(red: 230/255.0, green: 74/255.0, blue: 25/255.0, alpha: 1.0)
        
        
        if (expandedState == 0) {
                       
            if (selectedIndexPath.row < 14 && indexPath.row < 14) {
                
                self.revealViewController().revealToggleAnimated(true)
                
                linkListVC.selectKindTap(linkListVC.selectedTabIdx)
                
            } else {
                
                self.revealViewController().pushFrontViewController(destVC, animated: true)
            }
            
        } else {
            
            if (selectedIndexPath.row < 5 && indexPath.row < 5) {
                
                self.revealViewController().revealToggleAnimated(true)
                
                linkListVC.selectKindTap(linkListVC.selectedTabIdx)
                
            } else {
                
                self.revealViewController().pushFrontViewController(destVC, animated: true)
            }
        }
        
        selectedIndexPath = indexPath
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        searchField.resignFirstResponder()
    }
    
    @IBAction func profileTapped(sender: AnyObject) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .Default) { (alert: UIAlertAction) -> Void in
            self.openCamera()
        }
        let albumAction = UIAlertAction(title: "Gallery", style: .Default) { (alert: UIAlertAction!) -> Void in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (alert: UIAlertAction!) -> Void in
            
        }
        
        picker?.delegate = self
        
        alert.addAction(cameraAction)
        alert.addAction(albumAction)
        alert.addAction(cancelAction)
        presentViewController(alert, animated: true, completion: nil)
    }
    
    // open gallery
    func openGallery() {
        picker?.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        picker?.allowsEditing = true
        self.presentViewController(picker!, animated: true, completion: nil)
    }
    
    // open camera
    func openCamera() {
        
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
            
            picker?.sourceType = UIImagePickerControllerSourceType.Camera
            picker?.allowsEditing = true
            picker?.modalPresentationStyle = .FullScreen
            self.presentViewController(picker!, animated: true, completion: nil)
        }
    }
    
    // MARK: - UIImagePickerController Delegate
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let newImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            
            strPhotoPath = CommonUtils.saveToFile(newImage)
            
            imvProfile.contentMode = .ScaleAspectFit
            imvProfile.image = UIImage(contentsOfFile: strPhotoPath)
            
            self.uploadUserImage()
        }
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func uploadUserImage() {
        
        appDelegate.showLoadingViewWithTitle(nil, sender: self)
        
        let user_id = UserDefault.getIntValue(R.pref_user_id)
        let token = UserDefault.getString(R.pref_user_token)
        
        LinkAPI.addUserImage(user_id, token: token!, image_url: strPhotoPath
            , success: { (statusCode, message) in
                
                JLToast.makeText(message, duration: 1.0).show()
                
                self.getUserImage(user_id, token: token)
                
            }, failure:  { (statusCode, message) in
                
                self.appDelegate.hideLoadingView()
                
                JLToast.makeText(message, duration: 1.0).show()
        })
    }
    
    func getUserImage(user_id: Int!, token: String!) {
        
        LinkAPI.getUserImage(user_id, token: token
            , success: { (statusCode, message) in
                
                self.appDelegate.hideLoadingView()
                
                JLToast.makeText(message, duration: 1.0).show()
                
            }, failure:  { (statusCode, message) in
                
                self.appDelegate.hideLoadingView()
                
                JLToast.makeText(message, duration: 1.0).show()
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
