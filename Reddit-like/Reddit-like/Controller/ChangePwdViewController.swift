//
//  ChangePwdViewController.swift
//  Reddit-like
//
//  Created by Victory on 26/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit
import JLToast

class ChangePwdViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var imvUserImage: UIImageView!
    
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    // hamburger button
    @IBOutlet weak var menuButton: UIBarButtonItem!

    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        initView()
    }
    
    func initView() {
        
        // initialize side menu
        if self.revealViewController() != nil {
            
            if (self.revealViewController().rightViewController != nil) {
                self.revealViewController().rightViewController = nil
            }
            
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(self.revealViewController().revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        imvUserImage.setImageWithUrl(NSURL(string: UserDefault.getString(R.pref_user_image)!)!, placeHolderImage: UIImage(named: "user_icon.png"))
        
        // old password
        let oldPImageView = UIImageView(frame: CGRectMake(0, 0, 40, 36))
        oldPImageView.image = UIImage(named: "ic_lock")
        oldPImageView.contentMode = .Center
        
        // only left side corner radius
        let rectShape = CAShapeLayer()
        rectShape.bounds = oldPImageView.frame
        rectShape.position = oldPImageView.center
        rectShape.path = UIBezierPath(roundedRect: oldPImageView.bounds, byRoundingCorners: [.BottomLeft, .TopLeft], cornerRadii: CGSize(width: 5, height: 5)).CGPath
        
        oldPImageView.layer.backgroundColor = UIColor(red: 216/255.0, green: 67/255.0, blue: 21/255.0, alpha: 0.8).CGColor
        oldPImageView.layer.mask = rectShape
        
        let oldPView = UIView(frame: CGRectMake(0, 0, 46, 36))
        oldPView.addSubview(oldPImageView)
        
        txtOldPassword.leftView = oldPView
        txtOldPassword.leftViewMode = UITextFieldViewMode.Always
        
        // new Password
        let newPImageView = UIImageView(frame: CGRectMake(0, 0, 40, 36))
        newPImageView.image = UIImage(named: "ic_lock")
        newPImageView.contentMode = .Center
        
        newPImageView.layer.backgroundColor = UIColor(red: 216/255.0, green: 67/255.0, blue: 21/255.0, alpha: 0.8).CGColor
        newPImageView.layer.mask = rectShape
        
        let newPView = UIView(frame: CGRectMake(0, 0, 46, 36))
        newPView.addSubview(newPImageView)
        
        txtNewPassword.leftView = newPView
        txtNewPassword.leftViewMode = UITextFieldViewMode.Always
        
        // confirm Password
        let confirmPImageView = UIImageView(frame: CGRectMake(0, 0, 40, 36))
        confirmPImageView.image = UIImage(named: "ic_lock")
        confirmPImageView.contentMode = .Center
        
        confirmPImageView.layer.backgroundColor = UIColor(red: 216/255.0, green: 67/255.0, blue: 21/255.0, alpha: 0.8).CGColor
        confirmPImageView.layer.mask = rectShape
        
        let confirmPView = UIView(frame: CGRectMake(0, 0, 46, 36))
        confirmPView.addSubview(confirmPImageView)
        
        txtConfirmPassword.leftView = confirmPView
        txtConfirmPassword.leftViewMode = UITextFieldViewMode.Always
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
                
        super.viewWillAppear(animated)
    }
    
    func checkValid() -> Bool {
        
        if (txtOldPassword.text?.characters.count < 8 || txtOldPassword.text?.characters.count > 16) {
            
            appDelegate.showAlertDialog(nil, message: R.password_length, positive: R.alert_ok, negative: nil, sender: self)
            
            return false
            
        } else if (txtOldPassword.text != UserDefault.getString(R.pref_user_password)!) {
            
            appDelegate.showAlertDialog(nil, message: "Current password is wrong", positive: R.alert_ok, negative: nil, sender: self)
            
            return false
        } else if (txtNewPassword.text?.characters.count < 8 || txtNewPassword.text?.characters.count > 16) {
            
            appDelegate.showAlertDialog(nil, message: R.password_length, positive: R.alert_ok, negative: nil, sender: self)
            
            return false
            
        } else if (txtConfirmPassword.text?.characters.count < 8 || txtConfirmPassword.text?.characters.count > 16) {
            
            appDelegate.showAlertDialog(nil, message: R.password_length, positive: R.alert_ok, negative: nil, sender: self)
            
            return false
            
        } else if ( txtConfirmPassword.text != txtNewPassword.text) {
            
            appDelegate.showAlertDialog(nil, message: R.input_password_mismatch, positive: R.alert_ok, negative: nil, sender: self)
            
            return false
        }
        
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    // MARK: - UITextField delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if (textField == txtOldPassword) {
            
            txtNewPassword.becomeFirstResponder()
            
        } else if (txtNewPassword == textField) {
            
            txtConfirmPassword.becomeFirstResponder()
        }
        
        textField.resignFirstResponder()
        
        return true
    }
    
    @IBAction func changePasswordTapped(sender: AnyObject) {
        
        // check the validation of input value
        if (checkValid()) {
            
            let user_email = UserDefault.getString(R.pref_user_email)!
            let oldPassword = txtOldPassword.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            
            let newPassword = txtNewPassword.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            
            appDelegate.showLoadingViewWithTitle(nil, sender: self)
            
            // change password
            LinkAPI.changePassword(user_email, oldPassword: oldPassword, newPassword: newPassword
                , success: { (message) in
                    
                    self.appDelegate.hideLoadingView()
                    
                    JLToast.makeText(message, duration: 1.0).show()
                    
                }, failure: { (message) in
                    
                    
                    self.appDelegate.hideLoadingView()
                    
                    JLToast.makeText(message, duration: 1.0).show()
            })
        }
    }
}
 