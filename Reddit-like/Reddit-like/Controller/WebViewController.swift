//
//  WebViewController.swift
//  Reddit-like
//
//  Created by Victory on 06/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit
import JLToast

class WebViewController: UIViewController, UIWebViewDelegate {
    
    var loadItem: LinkModel!
    
    // web view
    @IBOutlet weak var webview: UIWebView!
    // circled x button
    @IBOutlet weak var imvXCircle: UIImageView!
    // circled check button
    @IBOutlet weak var imvCheckCircle: UIImageView!

    
    @IBOutlet weak var activityIndicator: BLLoader!
    
    @IBOutlet weak var viewComments: UIView!
    // comment count
    @IBOutlet weak var lblCommentCnt: UILabel!

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        initView()
    }
    
    func initView() {
        
        // navigation title label with two line & custom font
        // white bold & white attributes for title label
        let whiteBoldAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 17.0)!]
        let whiteAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Helvetica", size: 15.0)!]
        
        // navigation title label
        let lblTitle = UILabel(frame: CGRectMake(0, 0, 100, 44))
        lblTitle.backgroundColor = UIColor.clearColor()
        lblTitle.numberOfLines = 2
        lblTitle.textAlignment = .Center
        
        let attributedTitle = NSMutableAttributedString(string: "pics\("\n")\(loadItem.title)" as String)
        attributedTitle.addAttributes(whiteBoldAttributes, range: NSMakeRange(0, 4))
        attributedTitle.addAttributes(whiteAttributes, range: NSMakeRange(5, loadItem.title.characters.count))
        lblTitle.attributedText = attributedTitle
        
        self.navigationItem.titleView = lblTitle
        
        // loadItem model comment count
        viewComments.layer.borderColor = UIColor.whiteColor().CGColor
        lblCommentCnt.text = "View Comments (" + "\(loadItem.commentCnt)" + ")"
        
        // check and x button
        imvCheckCircle.image = imvCheckCircle.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        imvCheckCircle.tintColor = UIColor.whiteColor()
        
        imvXCircle.image = imvXCircle.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        imvXCircle.tintColor = UIColor.whiteColor()
        
        // change webview background color
        webview.backgroundColor = UIColor.clearColor()
        
        // initialize activity Indicator
        activityIndicator.lineWidth = 16
        activityIndicator.color = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
    }
    
    override func didReceiveMemoryWarning() {
     
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        // load request
        webview.loadRequest(NSURLRequest(URL: NSURL(string: loadItem.linkedURL)!))
        
        // start animating
        activityIndicator.startAnimation()
    }
    
    // MARK: UIWebView Delegate
    func webViewDidFinishLoad(webView: UIWebView) {
        
        if (activityIndicator.isAnimating()) {
            
            activityIndicator.stopAnimation()
            activityIndicator.hidden = true
        }
    }
    
    // share
    @IBAction func shareDidTapped(sender: AnyObject) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let favorite = UIAlertAction(title: loadItem.isFavorite ? R.share_unfavorite : R.share_favorite, style: .Default) { (alert: UIAlertAction!) -> Void in
            
            // set user link as favorite or unfavorite
            
            self.setUserLinkFavorite()
        }
        let copyurl = UIAlertAction(title: R.share_copy_url, style: .Default) { (alert: UIAlertAction!) -> Void in
            
            // copy url to clipboard
            UIPasteboard.generalPasteboard().string = self.loadItem.linkedURL
        }
        
        let shareopen = UIAlertAction(title: R.share_open, style: .Default) { (alert: UIAlertAction!) -> Void in
            
            // open url in safari
            UIApplication.sharedApplication().openURL(NSURL(string: self.loadItem.linkedURL)!)
        }
        
        let cancel = UIAlertAction(title: R.share_cencel, style: .Cancel) { (alert: UIAlertAction!) -> Void in
            
            // something here what you want
        }
        
        //        picker?.delegate = self

        alert.addAction(favorite)
        alert.addAction(copyurl)
        alert.addAction(shareopen)
        alert.addAction(cancel)
        presentViewController(alert, animated: true, completion: nil)
    }
    
    // done button
    @IBAction func doneDidTapped(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // back button
    @IBAction func backDidTapped(sender: AnyObject) {
        
        if (webview.canGoBack) {
            
            webview.goBack()
            
        } else {
            
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    // update user link -  dislike
    @IBAction func xbuttonDidTapped(sender: UIButton) {
        
        imvCheckCircle.image = imvCheckCircle.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        imvCheckCircle.tintColor = UIColor.whiteColor()
        imvCheckCircle.backgroundColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
        
        imvXCircle.image = imvXCircle.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        imvXCircle.tintColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
        imvXCircle.backgroundColor = UIColor.whiteColor()
        
        updateUserLink(-1)
    }
    
    // update user link -  like
    @IBAction func chckedButtonDidTapped(sender: UIButton) {
        
        imvCheckCircle.image = imvCheckCircle.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        imvCheckCircle.tintColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
        imvCheckCircle.backgroundColor = UIColor.whiteColor()
        
        
        imvXCircle.image = imvXCircle.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        imvXCircle.tintColor = UIColor.whiteColor()
        imvXCircle.backgroundColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
        
        
        // favorite the link
        updateUserLink(1)
    }
    
    // update user like value
    func updateUserLink(likeValue: Int!) {
        
        activityIndicator.hidden = false
        activityIndicator.startAnimation()
        
        let user_id = UserDefault.getIntValue(R.pref_user_id)
        let token = UserDefault.getString(R.pref_user_token)
        
        let linkid = loadItem.linkid
        
        LinkAPI.updateUserLink(user_id, token: token, linkid: linkid, likevalue: likeValue
            , success: { (statusCode, message) in
                
                JLToast.makeText(message, duration: 2.0).show()
                
                self.activityIndicator.stopAnimation()
                self.activityIndicator.hidden = true
            }
            , failure:  { (statusCode, message) in
                
                JLToast.makeText(message, duration: 2.0).show()
                
                self.activityIndicator.stopAnimation()
                self.activityIndicator.hidden = true
        })
    }
    
    func setUserLinkFavorite() {
        
        let isFavorite = !loadItem.isFavorite
        
        activityIndicator.hidden = false
        activityIndicator.startAnimation()
        
        let user_id = UserDefault.getIntValue(R.pref_user_id)
        let token = UserDefault.getString(R.pref_user_token)
        
        let linkid = loadItem.linkid
        
        LinkAPI.setUserLinkFavorite(user_id, token: token, linkid: linkid, isFavorite: isFavorite
            , success: { (statusCode, message) in
                
                JLToast.makeText(message, duration: 2.0).show()
                
                self.activityIndicator.stopAnimation()
                self.activityIndicator.hidden = true
                
                self.loadItem.isFavorite = isFavorite
            }
            , failure:  { (statusCode, message) in
                
                JLToast.makeText(message, duration: 2.0).show()
                
                self.activityIndicator.stopAnimation()
                self.activityIndicator.hidden = true
        })
    }
    
    // go to comment view
    @IBAction func viewCommentsTapped(sender: UIButton!) {
        
        let commentVC = self.storyboard?.instantiateViewControllerWithIdentifier("CommentViewController") as! CommentViewController
        commentVC.selectedItem = self.loadItem
        self.navigationController?.pushViewController(commentVC, animated: true)
    }
}
