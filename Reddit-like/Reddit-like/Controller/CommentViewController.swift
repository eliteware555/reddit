//
//  CommentViewController.swift
//  Reddit-like
//
//  Created by Victory on 06/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit
import RATreeView
import JLToast

class CommentViewController: UIViewController, RATreeViewDataSource, RATreeViewDelegate {
    
    var selectedItem: LinkModel!
    
    // comment data for testing
    // you have to make it with comments struct
    // main information - it contains username, userImage, date, score, body and it has replies (mean comment array)
    // replies (comment array) - it will be children of commentData ( each comment of replies has also their replies as a children)
    // I will make comment data array for testing now
    var commentData: [CommentModel] = []
    
    @IBOutlet weak var lblLinkTitle: UILabel!
    @IBOutlet weak var lblSource: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var lblCommentCount: UILabel!
    @IBOutlet weak var lblFavorite: UILabel!
    
    @IBOutlet weak var imvCircleCheck: UIImageView!
    @IBOutlet weak var imvCircleX: UIImageView!
    
    @IBOutlet weak var btnAddComment: UIButton!
    
    @IBOutlet weak var treeSuperView: UIView!
    
    // treeview to show comment
    var treeView: RATreeView!
    
    var replyModel: CommentModel!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        initVars()
        
        initView()
    }
    
    // initialize local variables
    func initVars() {
    }
    
    func initView() {
        
        // navigation title label with two line & custom font
        // white bold & white attributes for title label
        let whiteBoldAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 17.0)!]
        let whiteAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Helvetica", size: 15.0)!]
        
        // navigation title label
        let lblTitle = UILabel(frame: CGRectMake(0, 0, 100, 44))
        lblTitle.backgroundColor = UIColor.clearColor()
        lblTitle.numberOfLines = 2
        lblTitle.textAlignment = .Center
        
        let attributedTitle = NSMutableAttributedString(string: "pics\("\n")\(selectedItem.title)" as String)
        attributedTitle.addAttributes(whiteBoldAttributes, range: NSMakeRange(0, 4))
        attributedTitle.addAttributes(whiteAttributes, range: NSMakeRange(5, selectedItem.title.characters.count))
        lblTitle.attributedText = attributedTitle
        
        self.navigationItem.titleView = lblTitle
        
        // set selected item on view
        lblLinkTitle.text = selectedItem.title
        lblSource.text = selectedItem.source
        
        // show comment count
        updateCommentCount()
        
        // favorite icon - fontAwesome
        lblFavorite.font = UIFont.fontAwesomeOfSize(20.0)
        
        updateFavoriteIcon()
        
        // check and x button
        imvCircleCheck.image = imvCircleCheck.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        imvCircleCheck.tintColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
        
        imvCircleX.image = imvCircleX.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        imvCircleX.tintColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
        
        
        // LinkModel Image
        image.setImageWithUrl(NSURL(string: selectedItem.imageURL)!, placeHolderImage: UIImage(named: "3.png")!)
        
        btnAddComment.layer.borderColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0).CGColor
        
        // initialize tree view
        treeView = RATreeView(frame: treeSuperView.bounds)
        treeView.registerNib(UINib.init(nibName: "CommentCell", bundle: nil), forCellReuseIdentifier: "CommentCell")
        treeView.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.FlexibleWidth.rawValue | UIViewAutoresizing.FlexibleHeight.rawValue)
        treeView.delegate = self
        treeView.dataSource = self
        treeView.treeFooterView = UIView()
        treeView.backgroundColor = UIColor.clearColor()
        treeView.separatorColor = UIColor.clearColor()
        treeSuperView.addSubview(treeView)
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        getComments()
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    func updateCommentCount() {
        
        let commentCntLen = "\(selectedItem.commentCnt)".characters.count
        
        let attributedCommentCount = NSMutableAttributedString(string: selectedItem.commentCnt < 2 ? "\(selectedItem.commentCnt)" + " Comment" : "\(selectedItem.commentCnt)" + "Comments")
        
        attributedCommentCount.addAttributes([NSForegroundColorAttributeName: UIColor.blackColor()], range: NSMakeRange(0, commentCntLen))
        lblCommentCount.attributedText = attributedCommentCount
    }
    
    func updateFavoriteIcon() {
        
        if (selectedItem.isFavorite) {
            
            lblFavorite.text = String.fontAwesomeIconWithCode("fa-heart")!
            
        } else {
            
            lblFavorite.text = String.fontAwesomeIconWithCode("fa-heart-o")!
        }
    }
    
    @IBAction func doneTapped(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func favoriteTapped(sender: AnyObject) {
        
        setUserLinkFavorite()
    }
    
    func setUserLinkFavorite() {
        
        appDelegate.showLoadingViewWithTitle(nil, sender: self)
        
        let isFavorite = !selectedItem.isFavorite
        
        let user_id = UserDefault.getIntValue(R.pref_user_id)
        let token = UserDefault.getString(R.pref_user_token)
        
        let linkid = selectedItem.linkid
        
        LinkAPI.setUserLinkFavorite(user_id, token: token, linkid: linkid, isFavorite: isFavorite
            , success: { (statusCode, message) in
                
                JLToast.makeText(message, duration: 2.0).show()
                
                self.appDelegate.hideLoadingView()
                
                self.selectedItem.isFavorite = isFavorite
                
                self.updateFavoriteIcon()
                
            }
            , failure:  { (statusCode, message) in
                
                JLToast.makeText(message, duration: 2.0).show()
                
                self.appDelegate.hideLoadingView()
        })
    }
    
    @IBAction func addCommentAction(sender: AnyObject) {
        
        // go to comment view for adding a cooment
        let destVC = self.storyboard?.instantiateViewControllerWithIdentifier("PostViewController") as! PostViewController
        
        destVC.linkModel = self.selectedItem
        
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    @IBAction func circleCheckedTapped(sender: UIButton!) {
        
        imvCircleCheck.image = imvCircleCheck.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        imvCircleCheck.tintColor = UIColor.whiteColor()
        imvCircleCheck.backgroundColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
        
        
        imvCircleX.image = imvCircleX.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        imvCircleX.tintColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
        imvCircleX.backgroundColor = UIColor.whiteColor()
        
        updateUserLink(1)
    }
    
    @IBAction func circleXTapped(sender: UIButton) {
        
        imvCircleCheck.image = imvCircleCheck.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        imvCircleCheck.tintColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
        imvCircleCheck.backgroundColor = UIColor.whiteColor()
        
        imvCircleX.image = imvCircleX.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        imvCircleX.tintColor = UIColor.whiteColor()
        imvCircleX.backgroundColor = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 1.0)
        
        updateUserLink(-1)
    }
    
    func updateUserLink(likeValue: Int!) {
        
        appDelegate.showLoadingViewWithTitle(nil, sender: self)
        
        let user_id = UserDefault.getIntValue(R.pref_user_id)
        let token = UserDefault.getString(R.pref_user_token)
        
        let linkid = selectedItem.linkid
        
        LinkAPI.updateUserLink(user_id, token: token, linkid: linkid, likevalue: likeValue
            , success: { (statusCode, message) in
                
                JLToast.makeText(message, duration: 2.0).show()
                
                self.appDelegate.hideLoadingView()
            }
            , failure:  { (statusCode, message) in
                
                JLToast.makeText(message, duration: 2.0).show()
                
                self.appDelegate.hideLoadingView()
        })
    }
    
    @IBAction func shareTapped(sender: AnyObject) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let favorite = UIAlertAction(title: selectedItem.isFavorite ? R.share_unfavorite : R.share_favorite, style: .Default) { (alert: UIAlertAction!) -> Void in
            
            // set user link as favorite or unfavorite
            
            self.setUserLinkFavorite()
        }
        
        let copyurl = UIAlertAction(title: R.share_copy_url, style: .Default) { (alert: UIAlertAction!) -> Void in
        
            // copy url to clipboard
            UIPasteboard.generalPasteboard().string = self.selectedItem.linkedURL
        }
        
        let shareopen = UIAlertAction(title: R.share_open, style: .Default) { (alert: UIAlertAction!) -> Void in
            
            // open the url in safari
            UIApplication.sharedApplication().openURL(NSURL(string: self.selectedItem.linkedURL)!)
        }
        
        let cancel = UIAlertAction(title: R.share_cencel, style: .Cancel) { (alert: UIAlertAction!) -> Void in
            
            // something here what you want
        }
        
        //        picker?.delegate = self

        alert.addAction(favorite)
        alert.addAction(copyurl)
        alert.addAction(shareopen)
        alert.addAction(cancel)
        presentViewController(alert, animated: true, completion: nil)
    }
    
    func getComments() {
        
        appDelegate.showLoadingViewWithTitle(nil, sender: self)
        
        let user_id = UserDefault.getIntValue(R.pref_user_id)
        let token = UserDefault.getString(R.pref_user_token)
        
        let linkid = selectedItem.linkid
        
        LinkAPI.getComments(user_id, token:token, linkid: linkid, sortCategory: "score", sortOrder: "desc"
            , sucess: { (message) in
                
                self.appDelegate.hideLoadingView()
                
                self.commentData = message
                
//                self.updateCommentCount()
                
                self.treeView.reloadData()
            
            }
            , failure: { (message) in
                
                self.appDelegate.hideLoadingView()
                
                JLToast.makeText(message, duration: 2.0).show()
        })
    }
    
    func treeView(treeView: RATreeView, canEditRowForItem item: AnyObject) -> Bool {
        
        return false
    }
    
    // MARK: RATreeView data source
    func treeView(treeView: RATreeView, numberOfChildrenOfItem item: AnyObject?) -> Int {
        
        if let item = item as? CommentModel { // main comment - it has comment array (mean replies)
            
            return item.replies.count
            
        } else { // return comment data count
            
            return self.commentData.count
        }
    }
    
    // return comment model
    func treeView(treeView: RATreeView, child index: Int, ofItem item: AnyObject?) -> AnyObject {
        
        if let item = item as? CommentModel {
            
            // return comment model of replies at index
            return item.replies[index]
            
        } else {
            
            return commentData[index] as AnyObject
        }
    }
    
    // return cell
    func treeView(treeView: RATreeView, cellForItem item: AnyObject?) -> UITableViewCell {
    
        let cell = treeView.dequeueReusableCellWithIdentifier("CommentCell") as! CommentCell
        let item  = item as! CommentModel
        
        let level = treeView.levelForCellForItem(item)
        
        cell.setModel(item, level: level)
        
        cell.bodyTapActionBlock = { cell in
            
            self.showCommentSheet()
            
            let item = treeView.itemForCell(cell) as! CommentModel
            
            self.replyModel = item
        }
        
        return cell
    }
    
    func treeView(treeView: RATreeView, willExpandRowForItem item: AnyObject) {
        
        let item = item as! CommentModel
        
        let cell = treeView.cellForItem(item) as! CommentCell
        
        cell.setBody(item.comment_body)
    }
    
    func treeView(treeView: RATreeView, willCollapseRowForItem item: AnyObject) {
        
        let item = item as! CommentModel
        
        let cell = treeView.cellForItem(item) as! CommentCell
        
        cell.setBody("")
    }
    
    // MARK: RATreeView delegate
    func treeView(treeView: RATreeView, didSelectRowForItem item: AnyObject) {
        
    }
    
    func showCommentSheet() {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        // replay
        let reply_action = UIAlertAction(title: R.comment_reply, style: .Default) { (alert: UIAlertAction!) -> Void in
            
            // something here what you want
            // go to post view
            
            let destVC = self.storyboard?.instantiateViewControllerWithIdentifier("PostViewController") as! PostViewController
            
            destVC.isReply = true
            destVC.linkModel = self.selectedItem
            destVC.replyModel = self.replyModel
            
            
            self.navigationController?.pushViewController(destVC, animated: true)
        }
        
        // like
        let like_action = UIAlertAction(title: R.comment_like, style: .Default) { (alert: UIAlertAction!) -> Void in
            
            // something here what you want
            
            self.updateUserCommentScore(1)
        }
        
        // dislike
        let dislike_action = UIAlertAction(title: R.comment_dislike, style: .Default) { (alert: UIAlertAction!) -> Void in
            
            // something here what you want
            self.updateUserCommentScore(-1)
        }
        
        // flag
        let flag_action = UIAlertAction(title: R.comment_flag, style: .Default) { (alert: UIAlertAction!) -> Void in
            
            // something here what you want
            let flag_alert = UIAlertController(title: nil, message: R.flag_comment_alert, preferredStyle: UIAlertControllerStyle.Alert)
                
            flag_alert.addAction(UIAlertAction(title: R.alert_ok, style: UIAlertActionStyle.Default, handler: { (alert: UIAlertAction!) in
                
                self.flagComment("")
            }))
        
            flag_alert.addAction(UIAlertAction(title: R.alert_cancel, style: UIAlertActionStyle.Cancel, handler: nil))
            
            self.presentViewController(flag_alert, animated: true, completion: nil)
            
        }
        
        // cancel
        let cancel = UIAlertAction(title: R.share_cencel, style: .Cancel) { (alert: UIAlertAction!) -> Void in
            
            // something here what you want
        }
        
        //        picker?.delegate = self
        
        alert.addAction(reply_action)
        alert.addAction(like_action)
        alert.addAction(dislike_action)
        alert.addAction(flag_action)
        alert.addAction(cancel)
        presentViewController(alert, animated: true, completion: nil)
    }
    
    func updateUserCommentScore(likeValue: Int!) {
        
        appDelegate.showLoadingViewWithTitle(nil, sender: self)
        
        let user_id = UserDefault.getIntValue(R.pref_user_id)
        let token = UserDefault.getString(R.pref_user_token)
        
        let commentid = replyModel.commentId
        
        LinkAPI.updateUserCommentScore(user_id, token: token, commentid: commentid, likedvalue: likeValue
            , success: { (message) in
                
                self.appDelegate.hideLoadingView()

                JLToast.makeText(message, duration: 2.0).show()
            }
            ,failure:  { (message) in
               
                self.appDelegate.hideLoadingView()
                
                JLToast.makeText(message, duration: 2.0).show()
        })
        
    }
    
    func flagComment(commentBody: String!) {
        
        appDelegate.showLoadingViewWithTitle(nil, sender: self)
        
        let user_id = UserDefault.getIntValue(R.pref_user_id)
        let token = UserDefault.getString(R.pref_user_token)
        
        let commentid = replyModel.commentId
        
        LinkAPI.flagComment(user_id, token: token, commentid: commentid
            , success: { (message) in
                
                self.appDelegate.hideLoadingView()
                
                JLToast.makeText(message, duration: 2.0).show()
            
            }
            , failure:  { (message) in
          
                self.appDelegate.hideLoadingView()
                
                JLToast.makeText(message, duration: 2.0).show()
        })
    }
}





