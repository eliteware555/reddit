//
//  TopicModel.swift
//  Reddit-like
//
//  Created by Victory on 29/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import Foundation

class TopicModel: NSObject {
    
    var selected: Bool
    var item: String
    
    var subTopics:[TopicModel]
    
    override init() {
        
        selected = false
        item = ""
        
        subTopics = []
    }
    
    init (selected: Bool, item: String, subTopics: [TopicModel]) {
        
        self.selected = selected
        self.item = item
        
        self.subTopics = subTopics
    }

    convenience init(selected: Bool, item: String) {
        
        self.init(selected: selected, item: item, subTopics: [TopicModel]())
    }    
}
