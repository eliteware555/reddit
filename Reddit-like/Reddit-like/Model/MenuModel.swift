//
//  MenuModel.swift
//  Reddit-like
//
//  Created by Victory on 04/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit

class MenuModel {
    
    var lImage: UIImage!
    var rImage: UIImage!
    var item: String!
    
    var isSubMenu: Bool!
    
    init() {
        
        isSubMenu = false
    }
    
    init(lImage: UIImage!, rImage: UIImage, item: String, isSubMenu: Bool!) {
        
        self.lImage = lImage
        self.rImage = rImage
        self.item = item
        self.isSubMenu = isSubMenu;
    }
}
