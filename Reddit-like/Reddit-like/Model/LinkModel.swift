//
//  MainModel.swift
//  Reddit-like
//
//  Created by Victory on 04/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit

// It contains title, source, picture, date, comments count, etc.
// The link model should have an array of a Comments Struct

class LinkModel {
    
    var linkid: Int!
    var image: UIImage!
    var imageURL: String!
    var title: String!
    var source: String!
    var subDetail: String!  // description
    var commentCnt: Int!
    var linkedURL: String! // linked url - NSURL(fileURLWithPath: "")
    
    var views: Int!
    var score: Int!
    var likeValue: Int!
    var isFavorite: Bool

    var shortdate: String!
    var pubdate: String!
    var rssid: Int!
    
    init() {
        
        linkid = -1
        image = nil
        title = ""
        source = ""
        subDetail = ""
        commentCnt = 0
        linkedURL = ""
        
        // 
        views = 0
        score = 0
        likeValue = 0
        isFavorite = false
        shortdate = ""
        pubdate = ""
        rssid = 0
    }
    
    init (image: UIImage, title: String, source: String, subDetail: String, commentCnt: Int, linkedURL: String, isFavorite: Bool) {
        
        self.image = image
        self.title = title
        self.source = source
        self.subDetail = subDetail 
        self.commentCnt = commentCnt
        self.linkedURL = linkedURL
        self.isFavorite = isFavorite
    }
    
    
}