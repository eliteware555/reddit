//
//  CommentsModel.swift
//  Reddit-like
//
//  Created by Victory on 06/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

import Foundation

// Comments Model
// It contains username, userImage, date, source, body, replies
class CommentModel: NSObject {
    
   var user_name: String!
    var user_image_url: String!
    
    var comment_date: String!
    var comment_score: String!
    
    var comment_body: String!
    
    var isExpanded: Bool!
    
    var replies:[CommentModel] = [] // it will acts as a child of treeview
    
    var commentId: Int!
    var linkId: Int!
    var userId: Int!
    
    var comment_shortdate: String!
    
    var replyToCommentId: Int!
    
    override init() {
        
        user_name = ""
        user_image_url = ""
        
        comment_date = ""
        comment_body = ""
        comment_score = ""
        
        replies = []
        
        commentId = 0
        linkId = 0
        userId = 0
        
        comment_shortdate = ""
    }
    
    init(user_name: String, user_image: String, comment_date: String, comment_score: String, comment_body: String, replies: [CommentModel]) {
        
        self.user_name = user_name
        self.user_image_url = user_image
        self.comment_date = comment_date
        self.comment_score = comment_score
        self.comment_body = comment_body
        
        self.replies = replies
    }
    
    convenience init(user_name: String, user_image: String, comment_date: String, comment_score: String, comment_body: String) {
        
        self.init(user_name: user_name, user_image: user_image, comment_date: comment_date, comment_score: comment_score, comment_body: comment_body, replies: [CommentModel]())
    }
}
